package com.curos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.curos.dto.AccountDto;
import com.curos.service.AccountService;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "local")
@SpringBootTest
public class AccountRegisterTest {

	@Autowired
	AccountService accountService;
	

	@Test
	public void registerAccount() {
		// Account에 회원 저장
		// TODO : 회원가입 메일 전송 주석처리
		for(int i = 1; i < 207; i++) {
			AccountDto accountDto = settingAccountData(i);
			boolean result = accountService.memeberAccountJoin(accountDto);
			assertNotNull(accountDto.getAccountId());
			assertEquals(true, result);
		}
	}

	public AccountDto settingAccountData(int i) {
		AccountDto account = new AccountDto();
		account.setCompany("큐로스");
		account.setEmail("test"+i+"@curos.co.kr");
		account.setMobile("010-9123-0486");
		account.setName("테스트"+i);
		account.setPassword("asdf");
		account.setTelephone("010-9123-0486");
		
		return account;
	}
	
	@Test
	public void validatePassword() {
		boolean isPass = accountService.checkPassword("hsmini3@curos.co.kr", "curos788!", "curos788!@");
		assertTrue(isPass);
	}
}
