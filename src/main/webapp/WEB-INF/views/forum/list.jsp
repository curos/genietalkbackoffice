<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-6">
	  <div class="row margin-bottom">
	    <form id="forumListForm" action="/forumList" method="post">
	  	<div class="col-md-3">
          <select name="categoryId" class="form-control input-sm">
		  	<option value="">전체카테고리</option>
            <c:forEach items="${categoryList }" var="cate">
		  	  <option value="${cate['CATEGORY_ID']}" ${categoryId eq cate['CATEGORY_ID']?'selected':'' }>${cate['CATE_NAME']}</option>
            </c:forEach>
          </select> 
	  	</div>
	  	<div class="col-md-3">
          <select name="field" class="form-control input-sm">
		  	<option value="">모두</option>
		  	<option value="title" ${field eq 'title'?'selected':'' }>주제(제목)</option>
		  	<option value="question" ${field eq 'question'?'selected':'' }>질문(내용)</option>
          </select> 
	  	</div>
	  	<div class="col-md-6">
	  	<div class="input-group input-group-sm">
          <input type="text" name="keyword" class="form-control" value='${keyword}'>
              <span class="input-group-btn">
                <button type="submit" class="btn btn-info btn-flat">Search</button>
              </span>
        </div>
        </div>
        </form>
	  </div>
	  <div class="box">
	    <div class="box-body table-responsive no-padding">
	      <form id="clientForm" method="post" role="form" >
	      <table class="table">
	        <tbody><tr>
	          <th class="col-md-2">카테고리</th>
	          <th class="col-md-5">주제</th>
	          <th class="col-md-2">사용자</th>
	          <th class="col-md-1" style="text-align: center">답변</th>
	        </tr>
	        <c:forEach items="${forumList}" var="forum">
              <tr ${forum.useYn eq 'N'?'class="text-muted"':'' } >
                <td>${forum.categoryNm }</td>
                <td><a href="/forumDetail/${forum.develperForumId}">${forum.title }</a></td>
                <td>${forum.email }</td>
                <c:choose>
                  <c:when test="${forum.useYn eq 'N' }">
                  <td style="text-align: center">삭제됨</td>
                  </c:when>
                  <c:when test="${forum.answer != null && forum.answer != '' }">
                  <td style="text-align: center">답변완료</td>
                  </c:when>
                  <c:when test="${forum.answer == null || forum.answer == '' }">
                  <td style="text-align: center"><a href="javascript:answer(${forum.develperForumId })" class="btn btn-xs bg-primary"><b>등록</b></a></td>
                  </c:when>
                </c:choose>
                </tr>
	        </c:forEach>
	      </tbody>
	      </table>
	      </form>
	    </div>
	    <!-- /.box-body -->
	  </div>
	  <!-- /.box -->
	</div>
</div>
<div class="row">
  <div class="col-md-6">
	  <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate" style="text-align: center;">
	    <ul class="pagination">
	      <c:choose>
	        <c:when test="${page.firstBlock == 1}">
	          <li class="paginate_button previous disabled" ><a href="#">이전</a></li>
	        </c:when>
	        <c:otherwise>
	          <li class="paginate_button previous" ><a href="javascript:goPage(${page.firstBlock - 1})">이전</a></li>
	        </c:otherwise>
	      </c:choose>
	      <c:forEach var="num" begin="${page.firstBlock}" end="${page.lastBlock}">
	        <c:choose>
	          <c:when test="${num == page.currPage}">
			    <li class="paginate_button active"><a href="#" >${num }</a></li>
	          </c:when>
	          <c:otherwise>
	            <li class="paginate_button "><a href="javascript:goPage(${num})" >${num }</a></li>
	          </c:otherwise>
	        </c:choose>
	      </c:forEach>
	      <c:choose>
	        <c:when test="${page.lastBlock == page.totalPage}">
	          <li class="paginate_button next disabled" id="example2_next"><a href="#">다음</a></li>
	        </c:when>
	        <c:otherwise>
	          <li class="paginate_button next" id="example2_next"><a href="javascript:goPage(${page.lastBlock + 1})">다음</a></li>
	        </c:otherwise>
	      </c:choose>
	      
	    </ul>
	  </div>
	</div>
</div>
<script type="text/javascript">
	function goPage(currPage) {
		$('#forumListForm').append("<input type='hidden' value="+currPage+" name='currPage'>");
		$('#forumListForm').submit();
	}
	function answer(forumId) {
		window.location.href = '/forumAnswer/'+forumId;
	}
</script>