<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-lg-6 col-lg-offset-3" id="alert-div" style="display: none;">
		<div class="alert alert-danger alert-dismissible" id=alert-color-div role="alert">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">×</span>
				<span class="sr-only">Close</span>
			</button>
			<p id="alert-msg"></p>
		</div>
	</div>

	<div class="col-md-6">
	  <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <form id="addForumForm" method="post" role="form">
        <div class="box-body">
          <strong><i class="fa fa-map-marker margin-r-5"></i> 질문 주제</strong>
          <p class="text-muted"> <input type="text" class="form-control" placeholder="" name="title" id="title" required></p>          
          <hr>
          <strong><i class="fa fa-map-marker margin-r-5"></i> 카테고리</strong>
          <select name="categoryId" class="form-control input-sm">
		    <c:forEach items="${categoryList }" var="cate">
		  	  <option value="${cate['CATEGORY_ID']}">${cate['CATE_NAME']}</option>
            </c:forEach>
          </select> 
          <span class="block pad">문의하시는 이슈의 카테고리를 선택해주세요.</span>
          <hr>
          <strong><i class="fa fa-pencil margin-r-5"></i> 질문 내용</strong>
          <p class="text-muted"> <textarea class="form-control" placeholder="" name="question" id="question" rows="3" required></textarea></p>
          <hr>
        </div>
        <p class="pull-right">
	      <a href="/forumList" class="btn bg-primary margin"><b>목록</b></a>
	      <button type="submit" class="btn bg-primary margin" id="btn-add"><b>저장</b></button>
        </p>
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		$('#btn-add').click(function(event) {
			CommonUtil.postAjaxWithForm('addForumForm', '/forumReg'
					, function() {
						event.preventDefault();
						if($('#title').val().length > 100) {
							alert('질문 주제는 100글자 내외로 입력해주세요.');
							$('#title').focus();
							return false;
						}
						if(!confirm('등록하시겠습니까?')){
							return false;
						}
						return true;
					}
					, function(data) {
						alert('등록되었습니다.');
						window.location.href = '/forumList';
					}
					, function(data) {
						alert(data.resultMsg);
					}
			);
		});
	});
</script>