<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-6">
	  <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">개발자 포럼 상세보기</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <strong><i class="fa fa-book margin-r-5"></i> 질문 주제</strong>
          <p class="text-muted margin-bottom">${forum.title }</p>
          <strong><i class="fa fa-map-marker margin-r-5"></i> 카테고리</strong>
          <p class="text-muted margin-bottom"> ${forum.categoryNm }</p>
          <strong><i class="fa fa-question margin-r-5"></i> 질문 내용</strong>
          <p class="text-muted margin-bottom"> ${forum.question }</p>
          <c:if test="${forum.answer != null && forum.answer != '' }">
          <strong><i class="fa fa-pencil margin-r-5"></i> 답변 내용</strong>
          <p class="text-muted margin-bottom"> ${forum.answer }</p>
          </c:if>
          <hr>
        </div>
        <form id="forumForm" method="post" role="form" action="/myAppDel">
          <input type="hidden" name="forumId" value="${forum.develperForumId}">
        </form>
        <p class="pull-right">
	      <a href="/forumList" class="btn bg-primary margin"><b>목록</b></a>
          <c:if test="${forum.answer == null || forum.answer == '' }">
	      <button type="button" class="btn bg-primary margin" id="btn-answer">답변</button>
	      </c:if>
	      <button type="button" class="btn bg-primary margin" id="btn-mod">수정</button>
	      <c:if test="${forum.useYn eq 'Y' }">
	      <button type="button" class="btn bg-primary margin" id="btn-del">감추기</button>
	      </c:if>
	      <c:if test="${forum.useYn eq 'N' }">
	      <button type="button" class="btn bg-primary margin" disabled>삭제됨</button>
	      </c:if>
        </p>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		var error = '${error}';
		if(error) {
			alert(error);
			window.location.href = '/forumList';
		}
		
		$('#btn-del').click(function(event) {
			if(!confirm('삭제하시겠습니까?'))
				return;
			CommonUtil.postAjaxWithForm('forumForm', '/forumDel'
					, function() {
						event.preventDefault();
						return true;
					}
					, function(data) {
						alert('삭제되었습니다.');
						window.location.href = '/forumList';
					}
					, function(data) {
						alert(data.resultMsg);
					}
			);
		});
		
		$('#btn-mod').click(function(event) {
			window.location.href = '/forumModi/${forum.develperForumId}';
		});
		
		$('#btn-answer').click(function(event) {
			window.location.href = '/forumAnswer/${forum.develperForumId}';
		});
	});
</script>