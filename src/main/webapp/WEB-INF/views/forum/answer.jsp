<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-6">
	  <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">개발자 포럼 수정</h3>
        </div>
        <!-- /.box-header -->
        <form id="forumAnswerForm" method="post" role="form">
        <div class="box-body">
          <strong><i class="fa fa-book margin-r-5"></i> 질문 주제</strong>
          <p class="text-muted margin-bottom">${forum.title } </p>
          <strong><i class="fa fa-map-marker margin-r-5"></i> 카테고리</strong>
          <p class="text-muted margin-bottom">${forum.categoryNm }</p>
          <strong><i class="fa fa-question margin-r-5"></i> 질문 내용</strong>
          <p class="text-muted margin-bottom"> ${forum.question }</p>
          <strong><i class="fa fa-pencil margin-r-5"></i> 답변 내용</strong>
          <p class="text-muted margin-bottom"> <textarea class="form-control" placeholder="" name="answer" id="answer" rows="3" required></textarea></p>
        </div>
        <p class="pull-right">
	      <a href="/forumList" class="btn bg-primary margin"><b>목록</b></a>
          <a href="/forumDetail/${forum.develperForumId}"  class="btn bg-primary margin">상세</a>
          <button type="submit" class="btn bg-primary margin" id="btn-answer">저장</button>
        </p>
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		var error = '${error}';
		if(error) {
			alert(error);
			window.location.href = '/forumList';
		}

		$('#btn-answer').click(function(event) {
			CommonUtil.postAjaxWithForm('forumAnswerForm', '/forumAnswer/${forum.develperForumId}'
					, function() {
						event.preventDefault();
						if(!confirm('저장하시겠습니까?'))
							return false;
						return true;
					}
					, function(data) {
						alert('저장되었습니다.');
						window.location.href = '/forumDetail/${forum.develperForumId}';
					}
					, function(data) {
						alert(data.resultMsg);
					}
			);
		});
	});
</script>