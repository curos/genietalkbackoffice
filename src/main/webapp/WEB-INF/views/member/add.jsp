<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-6">
	  <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">회원 등록</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form id="addMemberForm" method="post" class="form-horizontal">
          <div class="box-body">
            <div class="form-group">
              <label for="email" class="col-sm-3 control-label">아이디(Email)</label>
              <div class="col-sm-9">
                <input type="email" class="form-control" id="email" name="email" placeholder="" required>
              </div>
            </div>
            <div class="form-group">
              <label for="company" class="col-sm-3 control-label">회사이름</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="company" name="company" placeholder="" required>
              </div>
            </div>
            <div class="form-group">
              <label for="name" class="col-sm-3 control-label">이름</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="name" name="name" placeholder="" required>
              </div>
            </div>
            <div class="form-group">
              <label for="mobile" class="col-sm-3 control-label">담당자 휴대폰</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="" required>
              </div>
            </div>
            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">회사 전화번호</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="telephone" name="telephone" placeholder="" required>
              </div>
            </div>
            <div class="form-group">
              <label for="postcode" class="col-sm-3 control-label">회사 우편번호</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="postcode" name="postcode" placeholder="" required>
              </div>
            </div>
            <div class="form-group">
              <label for="address1" class="col-sm-3 control-label">회사주소1</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="address1" name="address1" placeholder="" required>
              </div>
            </div>
            <div class="form-group">
              <label for="address2" class="col-sm-3 control-label">회사주소2</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="address2" name="address2" placeholder="" required>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="/memberList" class="btn btn-default"><b>취소</b></a>
            <button type="submit" class="btn bg-primary pull-right" id="btn-add"><b>등록</b></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		$('#btn-add').click(function(event) {
			CommonUtil.postAjaxWithForm('addMemberForm', '/memberReg'
					, function() {
						event.preventDefault();						
						if(!confirm('등록하시겠습니까?')){
							return false;
						}
						$('#btn-add').prop("disabled",true);
						return true;
					}
					, function(data) {
						alert('등록되었습니다.');
						window.location.href = '/memberList';
					}
					, function(data) {
						$('#btn-add').prop("disabled",false);
						alert(data.resultMsg);
					}
			);
		});
	});
</script>