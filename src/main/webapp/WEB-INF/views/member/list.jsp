<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
td{
padding:5px;
}
</style>
<div class="row margin-bottom">
	<div class="col-md-6">
		<form id="memberListForm" action="/memberList" method="post">
		<div class="col-md-3">
          <select name="field" class="form-control input-sm">
		  	<option value="email" ${field eq 'email'?'selected':'' }>계정</option>
		  	<option value="company" ${field eq 'company'?'selected':'' }>회사</option>
          </select> 
	  	</div>
	  	<div class="col-md-6">
	  	<div class="input-group input-group-sm">
     	     <input type="text" name="keyword" class="form-control" value='${keyword}'>
      	        <span class="input-group-btn">
     	           <button type="submit" class="btn btn-info btn-flat">Search</button>
     	         </span>
    	    </div>
        </div>
        </form>
        <div class="col-md-3">
		<a href="/memberReg" class="btn bg-primary pull-right"><b>등록</b></a>
		</div>
	</div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="box">
	  <div class="box-body table-responsive no-padding">
		<table class="table">
		    <tbody>
		        <tr>
		            <td class="col-md-3" >
						<strong>계정</strong>
		            </td>
		            <td class="col-md-2">
		                <strong>이름</strong>
		            </td>
		            <td class="col-md-4">
		                <strong>회사</strong>
		            </td >
		            <td class="col-md-3">
		                <strong>모바일</strong>
		            </td>		            
		        </tr>
		        <c:forEach items="${memberList}" var="member">
		        <tr valign="middle">
		            <td>
		        		<a href="/memberDetail/${member.accountId}">
		                	${member.email }
		                </a>
		            </td>
		            <td>
		            	${member.name }
		            </td>
		            <td>
		                ${member.company }
		            </td>
		            <td>
		                ${member.mobile }
		            </td>
		        </tr>
		        </c:forEach>
		    </tbody>
		</table>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
	  <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate" style="text-align: center;">
	    <ul class="pagination">
	      <c:choose>
	        <c:when test="${page.firstBlock == 1}">
	          <li class="paginate_button previous disabled" ><a href="#">이전</a></li>
	        </c:when>
	        <c:otherwise>
	          <li class="paginate_button previous" ><a href="javascript:goPage(${page.firstBlock - 1})">이전</a></li>
	        </c:otherwise>
	      </c:choose>
	      <c:forEach var="num" begin="${page.firstBlock}" end="${page.lastBlock}">
	        <c:choose>
	          <c:when test="${num == page.currPage}">
			    <li class="paginate_button active"><a href="#" >${num }</a></li>
	          </c:when>
	          <c:otherwise>
	            <li class="paginate_button "><a href="javascript:goPage(${num})" >${num }</a></li>
	          </c:otherwise>
	        </c:choose>
	      </c:forEach>
	      <c:choose>
	        <c:when test="${page.lastBlock == page.totalPage}">
	          <li class="paginate_button next disabled" id="example2_next"><a href="#">다음</a></li>
	        </c:when>
	        <c:otherwise>
	          <li class="paginate_button next" id="example2_next"><a href="javascript:goPage(${page.lastBlock + 1})">다음</a></li>
	        </c:otherwise>
	      </c:choose>
	      
	    </ul>
	  </div>
	</div>
</div>
<script>
function goPage(currPage)
{
	$('#memberListForm').append("<input type='hidden' value="+currPage+" name='currPage'>");
	$('#memberListForm').submit();
}
</script>