<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-6">
	  <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">회원 수정</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form id="modiMemberForm" method="post" class="form-horizontal">
          <div class="box-body">
            <div class="form-group">
              <label for="email" class="col-sm-3 control-label">아이디(Email)</label>
              <div class="col-sm-9">
                <input type="email" class="form-control" id="email" name="email" placeholder="" value="${member.email}" required>
              </div>
            </div>
            <div class="form-group">
              <label for="company" class="col-sm-3 control-label">회사이름</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="company" name="company" placeholder="" value="${member.company}" required>
              </div>
            </div>
            <div class="form-group">
              <label for="name" class="col-sm-3 control-label">이름</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="name" name="name" placeholder="" value="${member.name}" required>
              </div>
            </div>
            <div class="form-group">
              <label for="mobile" class="col-sm-3 control-label">담당자 휴대폰</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="" value="${member.mobile}" required>
              </div>
            </div>
            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">회사 전화번호</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="telephone" name="telephone" placeholder="" value="${member.telephone}" required>
              </div>
            </div>
            <div class="form-group">
              <label for="postcode" class="col-sm-3 control-label">회사 우편번호</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="postcode" name="postcode" placeholder="" value="${member.postcode}" required>
              </div>
            </div>
            <div class="form-group">
              <label for="address1" class="col-sm-3 control-label">회사주소1</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="address1" name="address1" placeholder="" value="${member.address1}" required>
              </div>
            </div>
            <div class="form-group">
              <label for="address2" class="col-sm-3 control-label">회사주소2</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="address2" name="address2" placeholder="" value="${member.address2}" required>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="/memberDetail/${member.accountId}" class="btn btn-default"><b>취소</b></a>
            <button type="submit" class="btn bg-primary pull-right" id="btn-mod"><b>수정</b></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		var error = '${error}';
		if(error) {
			alert(error);
			window.location.href = '/memberList';
		}
		$('#btn-mod').click(function(event) {
			CommonUtil.postAjaxWithForm('modiMemberForm', '/memberModi/${member.accountId}'
					, function() {
						event.preventDefault();						
						if(!confirm('수정하시겠습니까?')){
							return false;
						}
						return true;
					}
					, function(data) {
						alert('수정되었습니다.');
						window.location.href = '/memberDetail/${member.accountId}';
					}
					, function(data) {
						alert(data.resultMsg);
					}
			);
		});
	});
</script>