<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-6">
	  <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title">회원 상세</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form id="memberInfoForm" class="form-horizontal">
          <div class="box-body">
            <div class="form-group">
              <label for="email" class="col-sm-3 control-label">아이디(Email)</label>
              <div class="col-sm-9">
                <input type="email" class="form-control" id="company" value="${member.email}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="company" class="col-sm-3 control-label">회사이름</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="company" value="${member.company}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="name" class="col-sm-3 control-label">이름</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="name" value="${member.name}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="mobile" class="col-sm-3 control-label">담당자 휴대폰</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="mobile" value="${member.mobile}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">회사 전화번호</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="telephone"  value="${member.telephone}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="postcode" class="col-sm-3 control-label">회사 우편번호</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="postcode" value="${member.postcode}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="address1" class="col-sm-3 control-label">회사주소1</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="address1" value="${member.address1}" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="address2" class="col-sm-3 control-label">회사주소2</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="address2"  value="${member.address2}" readonly>
              </div>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="/memberList" class="btn btn-default"><b>목록</b></a>
            <button type="button" class="btn bg-primary pull-right" id="btn-mod"><b>수정</b></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		var error = '${error}';
		if(error) {
			alert(error);
			window.location.href = '/memberList';
		}
		
		$('#btn-mod').click(function(event) {
			window.location.href = '/memberModi/${member.accountId}';
		});
	});
</script>