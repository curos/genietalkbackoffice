<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<div id="header">
	<div class="logo">
			<a href="#"><img src="<%=request.getContextPath() %>/images/logo.png" alt="logo"/></a>
	</div>
	<div>
		<sec:authorize access="!isAuthenticated()">
			<a href="/login">로그인</a>
		</sec:authorize>
		<sec:authorize access="isAuthenticated()">
			<a href="/logout">로그아웃</a>
		</sec:authorize>
	</div>
</div>