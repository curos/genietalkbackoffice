<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="api_doc_response_code">
          <a href="/accountDetail">
            <i class="fa fa-th"></i> <span>개인 정보 수정</span>
           </a>
        </li>
        <li class="api_doc_lang_list">
          <a href="/memberList">
            <i class="fa fa-th"></i> <span>회원 관리</span>
          </a>
        </li>
        <li class="api_list">
          <a href="/apiList">
            <i class="fa fa-calendar"></i> <span>API 관리</span>
          </a>
        </li>
        <li class="change_password">
          <a href="/memberApiList">
            <i class="fa fa-calendar"></i> <span>API 사용량</span>
          </a>
        </li>
        <li class="forum_list forum_add forum_update forum_detail forum_answer">
          <a href="/forumList">
            <i class="fa fa-th"></i> <span>개발자포럼</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <script>
  $(function() {
		//menu
		$(".api_list ").click(function() {
			window.location.href = '/apiList';
		});
		$("."+"${menu}").addClass("active");
	});
  </script>