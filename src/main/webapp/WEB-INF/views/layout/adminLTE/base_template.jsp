<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><tiles:getAsString name="title"/></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<%=request.getContextPath() %>/components/bootstrap/3.3.7/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<%=request.getContextPath() %>/components/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<%=request.getContextPath() %>/components/ionicons/4.0.0-9/dist/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<%=request.getContextPath() %>/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<%=request.getContextPath() %>/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
  	<!-- Custom CSS -->
 	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/login.css">
 	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/change-pw.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/account.css">
  
  <!-- jQuery 3.3.1 -->
  <script src="<%=request.getContextPath() %>/components/jquery/3.3.1/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<%=request.getContextPath() %>/components/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<%=request.getContextPath() %>/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<%=request.getContextPath() %>/dist/js/demo.js"></script>
  
  <script src="<%=request.getContextPath() %>/js/CommonUtil.js"></script>
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper" style="background-color:white;">
		<tiles:insertAttribute name="header" />
		<div class="content-wrapper" style="margin-left:0px">
			<!-- Main content -->
		    <section class="content">
		      <tiles:insertAttribute name="body" />
		    </section>
		</div>
		<footer class="main-footer" style="margin-left:0px">
		<tiles:insertAttribute name="footer" />
		</footer>
	</div>
</body>
</html>