<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
	<div class="col-md-6">
		<div class="" id="alert-div" style="display: none;">
			<div class="alert alert-danger alert-dismissible" id=alert-color-div role="alert">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>
				<p id="alert-msg"></p>
			</div>
		</div>
		
		<div class="" id="change-div">
			<h4 class="">비밀번호 재확인</h4>
			<form accept-charset="UTF-8" role="form" id="checkPwForm" method="post" action="/checkPw">
				<fieldset>
					<span class="help-block">
					<font class="text-red">${msg }</font>
					회원님의 정보를 안전하게 보호하기 위해,
					<br> 개인정보를 수정하기 전에 비밀번호를 다시 한번 확인합니다.
					</span>
					<div class="form-group input-group">
						<span class="input-group-addon"> 비밀번호 </span> 
						<input class="form-control" placeholder="Password" name="password" id="password" type="password" required >
					</div>
					<button type="submit" class="btn btn-primary btn-block">확인</button>
				</fieldset>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		var err = '${err}';
		if(err == 1) {
			alert("비밀번호 불일치");
		}
	});
</script>