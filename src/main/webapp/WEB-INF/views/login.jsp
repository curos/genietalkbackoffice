<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="col-lg-4 col-lg-offset-4" id="alert-div" style="display: none;">
	<div class="alert alert-danger alert-dismissible" id=alert-color-div role="alert">
		<button type="button" class="close" data-dismiss="alert">
			<span aria-hidden="true">×</span>
			<span class="sr-only">Close</span>
		</button>
		<p id="alert-msg"></p>
	</div>
</div>

<div class="col-md-4 col-md-offset-4" id="form-olvidado">
	<div class="panel panel-login">
		<div class="panel-heading">
			<div class="row">
				<div class="col-xs-12">
					<a href="#" class="active" id="login-form-link">관리자 로그인</a>
				</div>
			</div>
			<hr>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-lg-12">
					<form id="login-form" action="/login" method="post" role="form" style="display: block;" >
						<div class="form-group">
							<input type="text" class="form-control" name="username" id="username" placeholder="Email" required />
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="password" placeholder="Password" required /> 
							<div id="countdown" style="display: none; color: red;"></div>
						</div>
						<div class="form-group text-center">
							<input type="checkbox" tabindex="3" class="" name="rememberMe" id="rememberMe">
							<label for="rememberMe"> 아이디 저장</label>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-sm-6 col-sm-offset-3">
									<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="로그인">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center">
										<a href="#" tabindex="5" class="forgot-password" id="forgetPwLink">비밀번호 찾기</a>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="col-md-4 col-md-offset-4" id="form-olvidado" style="display: none;">
	<h4 class="">임시 비밀번호 발급</h4>
	<form accept-charset="UTF-8" role="form" id="login-recordar" method="post" action="/forgotPw">
		<fieldset>
			<span class="help-block"> 당신의 이메일로 임시 비밀번호가 발송 됩니다. 
			<br> 발급된 임시 비밀번호로 로그인하여 비밀번호를 변경해 주세요.
			</span>
			<div class="form-group input-group">
				<span class="input-group-addon"> @ </span> 
				<input class="form-control" placeholder="Email" name="email" id="email" type="email" required >
			</div>
			<button type="submit" class="btn btn-primary btn-block" id="btn-olvidado">발송</button>
			<p class="help-block">
				<a class="text-muted" href="#" id="loginLink">
					<small>로그인</small>
				</a>
			</p>
		</fieldset>
	</form>
</div>


<script type="text/javascript">
	var error = '${param.error}';
	if (!error) {
		error = '${error}';
	}
	console.log('에러 코드 : ' + error);
	
	var success = '${success}';
	console.log('성공 코드 : ' + success);
	
	if (error) {
		var errorMsg = 'Error';
		if (error == 1) {
			errorMsg = 'Invalid username or password';
		} else if(error == 2) {
			errorMsg = 'Not Found Email Address';
		}
		$('#alert-msg').html(errorMsg);
		$('#alert-color-div').removeClass('alert-info');
		$('#alert-color-div').addClass('alert-danger');
		$('#alert-div').show();		
	} else if (success) {
		var successMsg = 'Success';
		if (success == 1) {
			successMsg = 'You have sent a temporary password via email.';
		}
		$('#alert-msg').html(successMsg);
		$('#alert-color-div').removeClass('alert-danger');
		$('#alert-color-div').addClass('alert-info');
		$('#alert-div').show();
		
		$('#countdown').show();
		CommonUtil.countdown("countdown", 10, 0, function() {
			$('#countdown').html('The temporary password has expired.');
		});
	}
	
	$(function() {

		var rememberMe = CommonUtil.getCookie('rememberMe');
		if (rememberMe) {
			$('#username').val(rememberMe);
			$('#rememberMe').prop('checked', true);
		}

		// 로그인 버튼 클릭
		$('#login-submit').click(function() {
			var isRememberMe = $('#rememberMe').prop('checked');
			if (isRememberMe) {
				var username = $('#username').val();
				CommonUtil.setCookie('rememberMe', username, 30);
			}
		});
		
		// 화면 토글		
		$('#forgetPwLink, #loginLink').click(function(e) {
			e.preventDefault();
			$('div#form-olvidado').toggle('500');
		});
		
		$('#btn-olvidado').click(function(event) {
			CommonUtil.postAjaxWithForm(
					'login-recordar'
					, '/forgotPw'
					, function() {
						event.preventDefault();
						return true;
					}
					, function(data) {
						alert('임시 패스워드가 발급 되었습니다.');
						$('div#form-olvidado').toggle('500');
					}
					, function(data) {
						alert('발급 실패 : ' + data.resultMsg);
					}
			);

		});
	});

</script>