<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="row">
	<div class="col-md-7">
	  <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">애플리케이션의 사용 현황을 조회합니다.</h3>
        </div>
        <!-- /.box-header -->
        <div class="row">
          <div class="box-body">
	        <div class="col-md-12">
	        <form id="searchForm" method="post" role="form">
	          <div class="form-group">
		        <div class="input-group">
		          <div class="input-group-addon">
		            <i class="fa fa-calendar"></i>
		          </div>
		          <input type="text" class="form-control pull-right" id="searchDate" name="searchDate" readonly>
		          <span class="input-group-btn">
	                 <button class="btn bg-primary">사용현황</button>
	              </span>
		        </div>
	          </div>
	        </form>
	        </div>
	      </div>
        </div>
        <div class="row">
          <div class="box-body">
            <div class="col-md-12">
              <strong><i class="fa fa-book margin-r-5"></i> 계정</strong>
              <p class="text-muted">${member.email }</p>
              <strong><i class="fa fa-book margin-r-5"></i> 이름</strong>
              <p class="text-muted">${member.name }</p>
              <strong><i class="fa fa-book margin-r-5"></i> 호출건(총/성공)</strong>
              <p class="text-muted"><fmt:formatNumber value="${total}" pattern="#,###" /> / <fmt:formatNumber value="${successTotal }" pattern="#,###" /></p>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="box-body">
            <div class="col-md-12">
	          <table class="table table-hover" id="analysisTable">
		        <tbody>
		        <tr>
		          <th>API 명</th>
		          <th>총 호출 수</th>
		          <th>호출 성공 수</th>
		          <th>사용가격</th>
		        </tr>
		        <c:forEach items="${result}" var="api">
		        <tr>
		          <td>${api['apiName']}</td>
		          <td><fmt:formatNumber value="${api['cnt']}" pattern="#,###" /></td>
		          <td><fmt:formatNumber value="${api['successCnt']}" pattern="#,###" /></td>
		          <td><fmt:formatNumber value="${api['price']}" pattern="#,###" /></td>		          
		        </tr>
		        </c:forEach>
		        </tbody>
	          </table>
	        </div>
          </div>
        </div>
        <div class="row">
          <div class="box-body">
            <div class="col-md-12">
        	    <button type="button" class="btn bg-primary pull-right" id="btn-excel"><b>Excel Down</b></button>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		var error = '${error}';
		if(error) {
			alert(error);
			window.location.href = '/memberApiList';
		}
		
		//Date range picker
		var start = '${start}';
		var end = '${end}';
	    $('#searchDate').daterangepicker({
			opens: 'left',
			locale: {
				format: 'YYYY/MM/DD'
			},
	   	    startDate: start,
	   	    endDate: end,
	   	    ranges: {
		   	    'Today': [moment(), moment()],
		   	    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		   	    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		   	    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		   	    'This Month': [moment().startOf('month'), moment().endOf('month')],
		   	    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	   	    }
	    });
	    
	    $('#btn-excel').click(function(event) {
	    	var accountId = '${member.accountId}';
	    	var searchDate = $("#searchDate").val();
	    	console.log(searchDate);
	    	var $form = $('<form></form>');
	    	$form.attr('method', 'post');
	    	$form.attr("action", "/memberApiDetailDownload/"+accountId);
	    	$form.appendTo('body');
	    	$form.append("<input type='hidden' value='"+searchDate+"' name='searchDate'>");
	    	$form.submit();
		});
	});	
</script>