<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-7">
	  <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">애플리케이션의 사용 현황을 조회합니다.</h3>
        </div>
        <!-- /.box-header -->
        <div class="row">
          <div class="box-body">
            <div class="col-md-12">
	        <form id="memberApiForm" method="post" role="form">
	          <div class="form-group">
		        <div class="input-group">
		          <div class="input-group-addon">
		            <i class="fa fa-calendar"></i>
		          </div>
		          <input type="text" class="form-control pull-right" id="searchDate" name="searchDate" readonly>
		          <span class="input-group-btn">
	                 <button class="btn bg-primary" id="btn-search">사용현황</button>
	              </span>
		        </div>
	          </div>
	        </form>
	        </div>
	      </div>
        </div>
        <div class="row">
          <div class="box-body">
            <div class="col-md-12">
	          <table class="table table-hover" id="analysisTable">
	            <tbody>
	              <tr>
	              	 <th class="col-md-1">#</th>
	              	 <th class="col-md-4">계정</th>
	              	 <th class="col-md-2">이름</th>
	              	 <th class="col-md-2">총 호출 건</th>
	              	 <th class="col-md-2">성공 호출 건</th>
	              </tr>
	              <c:forEach items="${memberApiList}" var="memberApi">
	              <tr>
	                 <td> ${memberApi.num} </td>
	                 <td><a href="javascript:goDetail(${memberApi.accountId})">${memberApi.email}</a></td>
	                 <td> ${memberApi.name} </td>
	                 <td> ${memberApi.cnt} </td>
	                 <td> ${memberApi.successCnt} </td>
	              </tr>
	              </c:forEach>
	            </tbody>
	          </table>
	        </div>
          </div>
        </div>
      </div>
    </div>
</div>
<div class="row">
  <div class="col-md-7">
	  <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate" style="text-align: center;">
	    <ul class="pagination">
	      <c:choose>
	        <c:when test="${page.firstBlock == 1}">
	          <li class="paginate_button previous disabled" ><a href="#">이전</a></li>
	        </c:when>
	        <c:otherwise>
	          <li class="paginate_button previous" ><a href="javascript:goPage(${page.firstBlock - 1})">이전</a></li>
	        </c:otherwise>
	      </c:choose>
	      <c:forEach var="num" begin="${page.firstBlock}" end="${page.lastBlock}">
	        <c:choose>
	          <c:when test="${num == page.currPage}">
			    <li class="paginate_button active"><a href="#" >${num }</a></li>
	          </c:when>
	          <c:otherwise>
	            <li class="paginate_button "><a href="javascript:goPage(${num})" >${num }</a></li>
	          </c:otherwise>
	        </c:choose>
	      </c:forEach>
	      <c:choose>
	        <c:when test="${page.lastBlock == page.totalPage}">
	          <li class="paginate_button next disabled" id="example2_next"><a href="#">다음</a></li>
	        </c:when>
	        <c:otherwise>
	          <li class="paginate_button next" id="example2_next"><a href="javascript:goPage(${page.lastBlock + 1})">다음</a></li>
	        </c:otherwise>
	      </c:choose>
	      
	    </ul>
	  </div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		//Date range picker
		var start = '${start}';
		var end = '${end}';
	    $('#searchDate').daterangepicker({
			opens: 'left',
			locale: {
				format: 'YYYY/MM/DD'
			},
	   	    startDate: start,
	   	    endDate: end,
	   	    ranges: {
		   	    'Today': [moment(), moment()],
		   	    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		   	    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		   	    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		   	    'This Month': [moment().startOf('month'), moment().endOf('month')],
		   	    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	   	    }
	    });
	});
	
	function goPage(currPage) {
		$('#memberApiForm').append("<input type='hidden' value="+currPage+" name='currPage'>");
		$('#memberApiForm').submit();
	}
	
	function goDetail(accountId) {		
		$("#memberApiForm").attr("action", "/memberApiDetail/"+accountId);
		$('#memberApiForm').submit();
	}
	
	
</script>