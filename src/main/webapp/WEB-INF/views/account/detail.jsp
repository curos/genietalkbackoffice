<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">아이디(이메일)</h3>
			</div>
			<div class="panel-body">${accountDto.email }</div>
			<!-- <div class="panel-footer">Panel footer</div> -->
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">회사이름</h3>
			</div>
			<div class="panel-body">${accountDto.company }</div>
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">이름</h3>
			</div>
			<div class="panel-body">${accountDto.name }</div>
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">담당자 휴대폰</h3>
			</div>
			<div class="panel-body">${accountDto.mobile }</div>
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">회사전화 번호</h3>
			</div>
			<div class="panel-body">${accountDto.telephone }</div>
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">회사 우편 번호</h3>
			</div>
			<div class="panel-body">${accountDto.postcode }</div>
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">회사주소1</h3>
			</div>
			<div class="panel-body">${accountDto.address1 }</div>
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">회사주소2</h3>
			</div>
			<div class="panel-body">${accountDto.address2 }</div>
		</div>
		
		<button type="button" class="btn btn-primary btn-block" id="btn-change">수정</button>
	</div>
</div>

<script type="text/javascript">
$(function() {
	$('#btn-change').click(function(event) {
		window.location.href = '/checkPw';
	});
})
</script>