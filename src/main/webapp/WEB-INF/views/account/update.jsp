<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 
<div class="row">
	<div class="col-md-6">
		<form id="modiAccountForm" method="post" role="form">
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">아이디(이메일)</h3>
			</div>
			<div class="panel-body">
				${accountDto.email }
			</div>
			<!-- <div class="panel-footer">Panel footer</div> -->
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">회사이름</h3>
			</div>
			<div class="panel-body">
				<input type="text" class="form-control" placeholder="" name="company" id="company" value="${accountDto.company }" required>
			</div>
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">이름</h3>
			</div>
			<div class="panel-body">
				<input type="text" class="form-control" placeholder="" name="name" id="name" value="${accountDto.name }" required>
			</div>
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">담당자 휴대폰</h3>
			</div>
			<div class="panel-body">
				<input type="text" class="form-control" placeholder="" name="mobile" id="mobile" value="${accountDto.mobile }" required>
			</div>
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">회사전화 번호</h3>
			</div>
			<div class="panel-body">
				<input type="text" class="form-control" placeholder="" name="telephone" id="telephone" value="${accountDto.telephone }" required>
			</div>
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">비밀 번호</h3>
			</div>
			<div class="panel-body">
				<button type="button" class="btn bg-gray btn-block" id="btn-pwChange">비밀번호 변경</button>
			</div>
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">회사 우편 번호</h3>
			</div>
			<div class="panel-body">
				<input type="text" class="form-control" placeholder="" name="postcode" id="postcode" value="${accountDto.postcode }" required>
			</div>
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">회사주소1</h3>
			</div>
			<div class="panel-body">
				<input type="text" class="form-control" placeholder="" name="address1" id="address1" value="${accountDto.address1 }" required>
			</div>
		</div>
		<div class="panel panel-default panel-horizontal">
			<div class="panel-heading">
				<h3 class="panel-title">회사주소2</h3>
			</div>
			<div class="panel-body">
				<input type="text" class="form-control" placeholder="" name="address2" id="address2" value="${accountDto.address2 }" required>
			</div>
		</div>
		
		<button type="submit" class="btn btn-primary btn-block" id="btn-change">수정</button>
		</form>
		
		<a href="#" data-toggle="modal" data-target="#login-modal" id="changePwModalLink" style="display: none;">Login</a>
		<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="loginmodal-container">
					<h1>비밀번호 변경</h1>
					<div class="card-body">
						<form class="form" id="changePwModalForm" role="form" autocomplete="off" action="/changePw" method="post">
							<div class="form-group">
								<label for="passwordOld">기존(임시) 비밀번호</label>
								<input type="password" class="form-control" id="passwordOld" name="passwordOld" required>
							</div>
							<div class="form-group">
								<label for="passwordNew">신규 비밀번호</label>
								<input type="password" class="form-control" id="passwordNew" name="passwordNew" pattern="(?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,16}" required>
								<span class="form-text small text-muted">
									영문,숫자,특수문자 조합, 6~16자리
								</span>
							</div>
							<div class="form-group">
								<label for="inputPasswordNewVerify">신규 비밀번호 확인</label>
								<input type="password" class="form-control" id="passwordNewConfirm" required>
							</div>
							<div class="form-group">
								<label for="securityCode">보안코드(6자리)</label>
								<input type="password" class="form-control" id="securityCode" name="securityCode" required>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success btn-lg float-right" id="changePwSubmit">변경</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(function() {
	$('#btn-change').click(function(event) {
		CommonUtil.postAjaxWithForm(
				'modiAccountForm'
				, '/accountModi'
				, function() {
					event.preventDefault();
					if(!confirm('수정하시겠습니까?')){
						return false;
					}
					return true;
				}
				, function(data) {
					alert('수정되었습니다.');
					window.location.href = '/accountDetail';
				}
				, function(data) {
					alert(data.resultMsg);
				}
		);
	});
	
	$('#btn-pwChange').click(function(event) {
		$.get("/changePw", function(data, status){
			alert('보안코드가 발송되었습니다.');
	        $('#changePwModalForm')[0].reset();
	        $('#changePwModalLink').trigger('click');
	    })
	    .fail(function() { alert("error"); });
	});

	$('#changePwSubmit').click(function(event) {
		CommonUtil.postAjaxWithForm(
				'changePwModalForm'
				, '/changePw'
				, function() {
					event.preventDefault();
					if ($('#passwordOld').val() == $('#passwordNew').val()) {
						alert('기존 비밀번호와 신규 비밀번호가 일치 합니다.\n신규 비밀번호를 다시 입력해 주세요.');
						$('#passwordNew').focus();
						return false;
					}
					
					if ($('#passwordNew').val() != $('#passwordNewConfirm').val()) {
						alert('신규 비밀번호 확인 값이 일치 하지 않습니다.');
						$('#passwordNewConfirm').focus();
						return false;
					}
					return true;
				}
				, function(data){
					alert('비밀번호가 변경 되었습니다.');
					$('#changePwModalLink').trigger('click');
				}
		);
	});
});

</script>