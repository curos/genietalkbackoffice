<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
td{
padding:5px;
}
</style>
<div class="row margin-bottom">
	<div class="col-md-6">
		<h1>
		    <a name="_Toc515531265">지니톡 자동 통역  API 목록</a>
		</h1>
		<table border="1">
		    <tbody>
		        <tr valign="middle">
		            <td width="158" >
						<strong>API 명</strong>
		            </td>
		            <td width="250">
		                <strong>URL</strong>
		            </td>
		            <td width="70">
		                <strong>가격</strong>
		            </td>
		            <td width="120">
		                <strong>등록일</strong>
		            </td>
		            <td width="120">
		                <strong>수정일</strong>
		            </td>
		        </tr>
		        <c:forEach items="${apiList}" var="api">
		        <tr valign="middle">
		            <td width="158">
		        		<a href="/apiDetail/${api.apiId}">
		                	${api.apiName }
		                </a>
		            </td>
		            <td width="250">
		            	${api.apiUrl }
		            </td>
		            <td width="70">
		                ${api.price }
		            </td>
		            <td width="120">
		                <fmt:formatDate value="${api.regDate }" pattern="yyyy.MM.dd" />
		            </td>
		            <td width="120">
		                <fmt:formatDate value="${api.modDate }" pattern="yyyy.MM.dd" />
		            </td>
		        </tr>
		        </c:forEach>
		    </tbody>
		</table>
	</div>		
</div>
<div class="row">
	<div class="col-md-6">
		<a href="/apiReg" class="btn bg-primary"><b>API 추가</b></a>
	</div>
</div>