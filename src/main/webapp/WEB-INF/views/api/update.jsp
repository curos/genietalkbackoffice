<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-6">
	  <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">API 기본적인 정보들을 등록합니다</h3>
        </div>
        <!-- /.box-header -->
        <form id="modApiForm" method="post" role="form">
        <input type="hidden" name="apiId" value="${apiInfo.apiId}">
        <div class="box-body">
          <strong><i class="fa fa-map-marker margin-r-5"></i> API 이름</strong>
          <p class="text-muted"> <input type="text" class="form-control" placeholder="" name="apiName" id="apiName" value="${apiInfo.apiName }" maxlength="20" required></p>
          <strong><i class="fa fa-map-marker margin-r-5"></i> API URL</strong>
          <p class="text-muted"> <input type="text" class="form-control" placeholder="" name="apiUrl" id="apiUrl" value="${apiInfo.apiUrl }" maxlength="50" required></p>
          <strong><i class="fa fa-map-marker margin-r-5"></i> API 가격</strong>
          <p class="text-muted"> <input type="number" class="form-control" placeholder="" name="price" id="price" value="${apiInfo.price }" max="2147483647" required></p>
          <strong><i class="fa fa-pencil margin-r-5"></i> API 설명</strong>
          <p class="text-muted"> <textarea class="form-control" placeholder="이 API 설명을 자세히 적으세요" name="descript" id="descript" required>${apiInfo.descript }</textarea></p>
        </div>
        <p class="pull-right">
	      <button type="submit" class="btn bg-primary margin" id="btn-mod"><b>수정</b></button>
	      <a href="/apiDetail/${apiInfo.apiId}" class="btn bg-primary margin"><b>취소</b></a>
        </p>
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		var error = '${error}';
		if(error) {
			alert(error);
			window.location.href = '/apiList';
		}
		
		$('#btn-mod').click(function(event) {
			CommonUtil.postAjaxWithForm('modApiForm', '/apiModi/${apiInfo.apiId}'
					, function() {
						event.preventDefault();
						if($('#apiName').val().length > 20) {
							alert('API 이름을 20글자 내외로 입력해주세요.');
							$('#apiName').focus();
							return false;
						}
						if($('#apiUrl').val().length > 50) {
							alert('API URL을 50글자 내외로 입력해주세요.');
							$('#apiUrl').focus();
							return false;
						}
						if(!confirm('수정하시겠습니까?')){
							return false;
						}
						return true;
					}
					, function(data) {
						alert('수정되었습니다.');
						window.location.href = '/apiDetail/${apiInfo.apiId}';
					}
					, function(data) {
						alert(data.resultMsg);
					}
			);
		});
	});
</script>