<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row">
	<div class="col-md-6">
	  <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">API 기본적인 정보들을 등록합니다</h3>
        </div>
        <!-- /.box-header -->
        <form id="addApiForm" method="post" role="form">
        <div class="box-body">
          <strong><i class="fa fa-map-marker margin-r-5"></i> API 이름</strong>
          <p class="text-muted"> <input type="text" class="form-control" placeholder="" name="apiName" id="apiName" maxlength="20" required></p>
          <strong><i class="fa fa-map-marker margin-r-5"></i> API URL</strong>
          <p class="text-muted"> <input type="text" class="form-control" placeholder="" name="apiUrl" id="apiUrl" maxlength="50" required></p>
          <strong><i class="fa fa-map-marker margin-r-5"></i> API 가격</strong>
          <p class="text-muted"> <input type="number" class="form-control" placeholder="" name="price" id="price" max="2147483647" required></p>
          <strong><i class="fa fa-pencil margin-r-5"></i> API 설명</strong>
          <p class="text-muted"> <textarea class="form-control" placeholder="이 API 설명을 자세히 적으세요" name="descript" id="descript" required></textarea></p>
        </div>
        <p class="pull-right">
	      <button type="submit" class="btn bg-primary margin" id="btn-add"><b>등록</b></button>
	      <a href="/apiList" class="btn bg-primary margin"><b>취소</b></a>
        </p>
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		$('#btn-add').click(function(event) {
			CommonUtil.postAjaxWithForm('addApiForm', '/apiReg'
					, function() {
						event.preventDefault();
						if($('#apiName').val().length > 20) {
							alert('API 이름을 20글자 내외로 입력해주세요.');
							$('#apiName').focus();
							return false;
						}
						if($('#apiUrl').val().length > 50) {
							alert('API URL을 50글자 내외로 입력해주세요.');
							$('#apiUrl').focus();
							return false;
						}
						if(!confirm('등록하시겠습니까?')){
							return false;
						}
						return true;
					}
					, function(data) {
						alert('등록되었습니다.');
						window.location.href = '/apiList';
					}
					, function(data) {
						alert(data.resultMsg);
					}
			);
		});
	});
</script>