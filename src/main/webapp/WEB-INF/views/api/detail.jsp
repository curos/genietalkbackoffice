<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>    	
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="row">
	<div class="col-md-6">
	  <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">API 정보들을 조회/수정합니다</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <strong><i class="fa fa-book margin-r-5"></i> API 이름</strong>
          <p class="text-muted margin-bottom">${apiInfo.apiName }</p>
          <strong><i class="fa fa-map-marker margin-r-5"></i> API URL</strong>
          <p class="text-muted margin-bottom"> ${apiInfo.apiUrl }</p>
          <strong><i class="fa fa-map-marker margin-r-5"></i> API 가격</strong>
          <p class="text-muted margin-bottom"> ${apiInfo.price }</p>
          <strong><i class="fa fa-pencil margin-r-5"></i> 애플리케이션 설명</strong>
          <p class="text-muted margin-bottom"> ${apiInfo.descript }</p>
          <strong><i class="fa fa-map-marker margin-r-5"></i> 등록일</strong>
          <p class="text-muted margin-bottom"> <fmt:formatDate value="${apiInfo.regDate }" pattern="yyyy-MM-dd" /></p>
          <strong><i class="fa fa-map-marker margin-r-5"></i> 수정일</strong>
          <p class="text-muted margin-bottom"> <fmt:formatDate value="${apiInfo.modDate }" pattern="yyyy-MM-dd" /></p>
        </div>
        <form id="clientForm" method="post" role="form" action="/myAppDel">
          <input type="hidden" name="apiId" value="${apiInfo.apiId}">
        </form>
        <p class="pull-right">
	      <button type="button" class="btn bg-primary margin" id="btn-mod">수정</button>
	      <a href="/apiList" class="btn bg-primary margin"><b>목록</b></a>
        </p>
      </div>
    </div>
</div>

<script type="text/javascript">
	$(function() {
		var error = '${error}';
		if(error) {
			alert(error);
			window.location.href = '/apiList';
		}
		$('#btn-mod').click(function(event) {
			window.location.href = '/apiModi/${apiInfo.apiId}';
		});
	});
</script>