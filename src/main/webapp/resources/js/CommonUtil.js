
if("undefined"==typeof jQuery) {
	throw new Error("CommonUtil requires jQuery");	
}

(function CommonUtil(window) {
	var commonUtil = {

		/*
		 * POST Ajax 요청 (form parameter 방식)
		 */
		postAjaxWithForm : function(formId, url, validCallback, successCallback, failCallback) {
			var form = jQuery( "#" + formId );
			
			// 폼 유효성 검사
			if (form[0].checkValidity()) {
				if (validCallback) {
					if (!validCallback()) {
						return;
					}					
				}
				
				var formVars = form.serialize();
				console.log(formVars);
				
				$.post(url, formVars, function(data, textStatus) {
					console.log(data);
					console.log('success status : ' + textStatus);
					if (data.resultCode != 1) {
						if (failCallback) {
							failCallback(data);
						} else {
							alert(data.resultMsg);								
						}
					} else {
						if (successCallback) {
							successCallback(data);								
						} else {
							alert(data);
						}
					}
				})
				.done(function(data, textStatus, jqXHR) {
					console.log('seconsd success');
				})
				.fail(function(data, textStatus, jqXHR) {
					console.log('fail status : ' + textStatus);
					if (failCallback) {
						failCallback(data);						
					} else {
						alert(data);
					}
				})
				.always(function(data, textStatus, jqXHR) {
					console.log('complete');
				});
			}
		}
	
		/*
		 * 쿠키 조회
		 */
		,getCookie : function(cookieName) {
			var theCookie = "" + document.cookie;
			var ind = theCookie.indexOf(cookieName + "=");
			if (ind == -1 || cookieName == "") {
				return "";
			}
			var ind1 = theCookie.indexOf(";", ind);
			if (ind1 == -1) {
				ind1 = theCookie.length;
			}
			return unescape(theCookie.substring(ind + cookieName.length + 1, ind1));
		}
		
		// 쿠키 저장
		,setCookie : function(cookieName, cookieValue, nDays) {
			var today = new Date();
			var expire = new Date();
			if (nDays == null || nDays == 0) {
				nDays = 1;
			}
			expire.setTime(today.getTime() + 3600000 * 24 * nDays);
			document.cookie = cookieName + "=" + escape(cookieValue) + ";expires="
					+ expire.toGMTString();
		}
		
		// 카운트 다운
		,countdown : function(elementName, minutes, seconds, timeoutCallback) {
		    var element, endTime, hours, mins, msLeft, time;

		    function twoDigits( n ) {
		        return (n <= 9 ? "0" + n : n);
		    }

		    function updateTimer() {
		        msLeft = endTime - (+new Date);
		        if ( msLeft < 1000 ) {
		        	if (timeoutCallback) {
		        		timeoutCallback();
		        	} else {
		        		element.innerHTML = "Time Over!";		        		
		        	}
		        } else {
		            time = new Date( msLeft );
		            hours = time.getUTCHours();
		            mins = time.getUTCMinutes();
		            element.innerHTML = (hours ? hours + ':' + twoDigits( mins ) : mins) + ':' + twoDigits( time.getUTCSeconds() );
		            setTimeout( updateTimer, time.getUTCMilliseconds() + 500 );
		        }
		    }

		    element = document.getElementById( elementName );
		    endTime = (+new Date) + 1000 * (60*minutes + seconds) + 500;
		    updateTimer();
		}
	
	};
	window.CommonUtil = commonUtil; 
})(window);
