package com.curos.dto;

import java.util.Date;

public class AccountDto {
	private int accountId;
	private String email;
	private String password;;
	private String name;
	private String company;
	private String mobile;
	private String telephone;
	private Date regDate;
	private String tempPasswd;
	private Date tempPasswdDate;
	private String secCodeSix;
	private String postcode;
	private String address1;
	private String address2;
	
	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getTempPasswd() {
		return tempPasswd;
	}

	public void setTempPasswd(String tempPasswd) {
		this.tempPasswd = tempPasswd;
	}

	public Date getTempPasswdDate() {
		return tempPasswdDate;
	}

	public void setTempPasswdDate(Date tempPasswdDate) {
		this.tempPasswdDate = tempPasswdDate;
	}

	public String getSecCodeSix() {
		return secCodeSix;
	}

	public void setSecCodeSix(String secCodeSix) {
		this.secCodeSix = secCodeSix;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
}