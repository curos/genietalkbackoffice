package com.curos.dto;

import java.util.Date;

public class ApiDto {
	private int apiId;
	private String apiName;
	private String apiUrl;
	private String descript;
	private int price;
	private Date regDate;
	private Date modDate;

	public int getApiId() {
		return apiId;
	}

	public void setApiId(int apiId) {
		this.apiId = apiId;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}

	public String getDescript() {
		return descript;
	}

	public void setDescript(String descript) {
		this.descript = descript;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public Date getModDate() {
		return modDate;
	}

	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}

	@Override
	public String toString() {
		return "ApiDto [apiId=" + apiId + ", apiName=" + apiName + ", apiUrl=" + apiUrl + ", descript=" + descript
				+ ", price=" + price + ", regDate=" + regDate + ", modDate=" + modDate + "]";
	}
	
}