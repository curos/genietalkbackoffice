package com.curos.dao;

import java.util.List;
import java.util.Map;

import com.curos.dto.ApiDto;

public interface ApiMapper {
	public List<ApiDto> selectApiAllList();
	public ApiDto selectApiByApiId(int apiId);
	public int insertApiPriceHistory(ApiDto apiDto);
	public int insertApi(ApiDto apiDto);
	public int updateApi(ApiDto apiDto);
	public int insertAccountHasApi(int apiId);
	public List<Map<String,Object>> selectAnalysisList(Map<String, Object> paramMap);
	public List<Map<String, Object>> selectAnalysisByAccountId(Map<String, Object> paramMap);
	public List<Map<String, Object>> selectAnalysisDetailByAccountId(Map<String, Object> paramMap);
}
