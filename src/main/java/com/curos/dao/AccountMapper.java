package com.curos.dao;

import java.util.List;
import java.util.Map;

import com.curos.dto.AccountDto;

public interface AccountMapper {
	public int insertAccount(AccountDto account);
	public int insertAccountHasAccountRole(Map<String, Object> accountData);
	public int insertAccountHasLanguage(int accountId);
	public int insertAccountHasApi(int accountId);
	public AccountDto selectAccountByEmail(String email);
	public AccountDto selectAccountByAccountId(int accountId);
	public int updateAccountTempPassword(Map<String, Object> paramMap);
	public int updateSecurityCode(Map<String, String> paramMap);
	public int updatePassword(Map<String, Object> paramMap);
	public List<Integer> selectAccountHasRole(int accountId);
	public int updateAccountByEmail(AccountDto account);
	public int updateAccountByAccountId(AccountDto account);
	public int getMemberCount(Map<String, Object> searchMap);
	public List<AccountDto> selectMemberList(Map<String, Object> searchMap);
}
