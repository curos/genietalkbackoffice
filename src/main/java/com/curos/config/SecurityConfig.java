package com.curos.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.curos.constant.UrlConstant;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	UserDetailsService userService;
	@Override
	public void configure(WebSecurity web) throws Exception
	{
		web.ignoring()
				.antMatchers("/css/**")
				.antMatchers("/images/**")
				.antMatchers("/js/**")
				.antMatchers("/components/**")
				.antMatchers("/dist/**")
		;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf().disable()
	        // 사용자의 쿠키에 세션을 저장하지 않음
	        // Rest 아키텍쳐는 stateless를 조건으로 하기 때문 stateless 조건을 사용하는게 맞으나
	        // NEVER는 security 등 내부적으로 세션을 만드는 것을 허용
	        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER)
	        .and()
				.authorizeRequests()
					.antMatchers("/").permitAll()
					.antMatchers("/forgotPw").permitAll()
					.anyRequest().authenticated()
			.and()
				.formLogin()// 로그인 설정
					.loginPage(UrlConstant.LOGIN)
					.loginProcessingUrl(UrlConstant.LOGIN)
					.defaultSuccessUrl("/main", true)
					.failureUrl(UrlConstant.LOGIN+"?error=1")
					.permitAll()
			.and()
				.logout()// 로그아웃 설정
					.logoutUrl(UrlConstant.LOGOUT)
					.invalidateHttpSession(true)
					.logoutSuccessUrl("/")
			;
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		CustomDaoAuthenticationProvider provider = new CustomDaoAuthenticationProvider();
		provider.setUserDetailsService(userService);
		provider.setPasswordEncoder(passwordEncoder());
		auth.authenticationProvider(provider);
	}
	
	@Bean
    public PasswordEncoder passwordEncoder(){
		//return NoOpPasswordEncoder.getInstance();
		// TODO : 추후에 적용 예정
		return new BCryptPasswordEncoder();
    }
	
	public static class CustomDaoAuthenticationProvider extends DaoAuthenticationProvider {
		
		public CustomDaoAuthenticationProvider() {
			super();
		}
		
		@Override
		public void additionalAuthenticationChecks(UserDetails userDetails,
				UsernamePasswordAuthenticationToken authentication)
				throws AuthenticationException {
			if (authentication.getCredentials() == null) {
				logger.debug("Authentication failed: no credentials provided");

				throw new BadCredentialsException(messages.getMessage(
						"AbstractUserDetailsAuthenticationProvider.badCredentials",
						"Bad credentials"));
			}
			
			String presentedPassword = authentication.getCredentials().toString();
			String[] passwords = userDetails.getPassword().split(" ");
			if (passwords.length == 1 
					&& !super.getPasswordEncoder().matches(presentedPassword, passwords[0])) {
				logger.debug("Authentication failed: password does not match stored value");
				throw new BadCredentialsException(messages.getMessage(
						"AbstractUserDetailsAuthenticationProvider.badCredentials",
						"Bad credentials"));
			}
			else if (passwords.length == 2) {
				if (super.getPasswordEncoder().matches(presentedPassword, passwords[0])) {
					// 비번 일치
				} else if(super.getPasswordEncoder().matches(presentedPassword, passwords[1])) {
					authentication.setDetails("TEMP_PW_LOGIN");
				} else {
					logger.debug("Authentication failed: password does not match stored value");
					throw new BadCredentialsException(messages.getMessage(
							"AbstractUserDetailsAuthenticationProvider.badCredentials",
							"Bad credentials"));					
				}
			}
			
			
		}
	}

}
