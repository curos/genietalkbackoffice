package com.curos.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtils {

	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
	public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd");

	/**
	 * 현재 날짜 시간
	 * @return
	 */
	public static LocalDateTime getNowDateTime() {
		return LocalDateTime.now();
	}
	
	/**
	 * 현재 날짜 시간 문자열
	 * @return
	 */
	public static String getNowDateTimeStr() {
		return getNowDateTime().format(DATE_TIME_FORMATTER);
	}
	
	/**
	 * 현재 날짜
	 * @return
	 */
	public static LocalDate getNowDate() {
		return LocalDate.now();
	}
	
	/**
	 * 현재 날짜 문자열
	 * @return
	 */
	public static String getNowDateStr() {
		return getNowDate().format(DATE_FORMATTER);
	}
	
	
}
