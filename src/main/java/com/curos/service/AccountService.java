package com.curos.service;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.mail.MessagingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.curos.constant.Role;
import com.curos.dao.AccountMapper;
import com.curos.dto.AccountDto;
import com.curos.dto.ApiDto;
import com.curos.util.CustomStringUtils;

import freemarker.template.TemplateException;

@Service
@Transactional
public class AccountService {
	
	@SuppressWarnings("unused")
	private final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private ApiService apiService;
	
	@Autowired
	private AccountMapper accountMapper;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	public boolean memeberAccountJoin(AccountDto account) {
		String password = account.getPassword();

		//Account 저장
		if (registerAccount(account) < 1) {
			return false;
		}

		//Role 저장
		Map<String, Object> accountData = new HashMap<String, Object>();
		accountData.put("accountId", account.getAccountId());
		accountData.put("accountRoleId", Role.ROLE_DEVELOP.getRoleId());

		if (accountMapper.insertAccountHasAccountRole(accountData) < 1) {
			return false;
		}

		// TODO : 사용 가능 언어 저장 (수정 필요)
		if (accountMapper.insertAccountHasLanguage(account.getAccountId()) != 9) {
			return false;
		}

		// TODO : 사용가능 API 저장 (수정 필요)
		List<ApiDto> apiList = apiService.selectApiAllList();
		if (accountMapper.insertAccountHasApi(account.getAccountId()) != apiList.size()) {
			return false;
		}

		// Email 발송
		try {
			emailService.sendMemberJoin(account.getEmail(), password);
		} catch (MessagingException | IOException | TemplateException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	private int registerAccount(AccountDto account) {
		String encodedPassword = passwordEncoder.encode(account.getPassword());
		account.setPassword(encodedPassword);
		return accountMapper.insertAccount(account);
	}
	
	/**
	 * 계정 정보 조회
	 * @param email
	 * @return
	 */
	public AccountDto getAccountByEmail(String email) {
		return accountMapper.selectAccountByEmail(email);
	}
	public AccountDto getAccountByAccountId(int accountId) {
		return accountMapper.selectAccountByAccountId(accountId);
	}

	public boolean checkAccountHasRole(int accountId, Role role) {
		List<Integer> roleList = accountMapper.selectAccountHasRole(accountId);
		if (roleList.contains(role.getRoleId())) {
			return true;
		}
		return false;
	}
	
	/**
	 * 계정 정보 존재 유무
	 * @param email
	 * @return
	 */
	public boolean isExistAccount(String email) {
		return getAccountByEmail(email) == null ? false : true;
	}

	/**
	 * 임시 비밀번호 업데이트
	 * @param email
	 * @param tempPassword
	 */
	public void updateTempPassword(String email, String tempPassword) {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("email", email);
		paramMap.put("tempPassword", passwordEncoder.encode(tempPassword));
		paramMap.put("tempPasswordDate", getCurrentDate(10));
		accountMapper.updateAccountTempPassword(paramMap);
	}
	
	/**
	 * 현재 시간 + minute
	 * @param minute
	 * @return
	 */
	private Date getCurrentDate(int minute) {
		final long ONE_MINUTE_IN_MILLIS = 60000;
		Calendar date = Calendar.getInstance();
		long t = date.getTimeInMillis();
		return new Date(t + (minute * ONE_MINUTE_IN_MILLIS));
	}

	/**
	 * 비밀번호 일치 여부
	 * @param email
	 * @param password
	 * @param newPassword
	 * @return
	 */
	public boolean equalsPassword(String email, String password) {
		Optional<AccountDto> optional = Optional.ofNullable(accountMapper.selectAccountByEmail(email));
		
		// 기존 비밀번호 일치 여부
		if (optional.filter(dto -> passwordEncoder.matches(password, dto.getPassword())).isPresent()) {
			return true;
		}
		
		// 임시 비밀번호 일치 여부
		return optional
				.filter(dto -> dto.getTempPasswd() != null && dto.getTempPasswdDate() != null)
				.filter(dto -> dto.getTempPasswdDate().after(new Date()))
				.filter(dto -> passwordEncoder.matches(password, dto.getTempPasswd()))
				.isPresent();
	}
	
	/**
	 * 비밀번호 체크
	 * @param email
	 * @param password
	 * @param newPassword
	 * @return
	 */
	public boolean checkPassword(String email, String password, String newPassword) {
		
		// 기존 비번과 동일한 신규 비번으로 변경 불가
		if (StringUtils.equals(password, newPassword)) {
			return false;
		}
		
		// 비밀번호 규칙 검사
		if (!CustomStringUtils.validatePasswordRule(newPassword)) {
			return false;
		}
		
		// 비밀번호 일치 여부
		return equalsPassword(email, password);
	}

	/**
	 * 보안 코드 업데이트
	 * @param email
	 * @param securityCode
	 */
	public void updateSecurityCode(String email, String securityCode) {
		Map<String,String> paramMap = new HashMap<>();
		paramMap.put("email", email);
		paramMap.put("securityCode", securityCode);
		accountMapper.updateSecurityCode(paramMap);
	}

	/**
	 * 보안 코드 체크
	 * @param securityCode
	 * @return
	 */
	public boolean checkSecurityCode(String email, String securityCode) {
		return Optional.ofNullable(getAccountByEmail(email))
				.filter(dto -> {
					return StringUtils.equals(dto.getSecCodeSix(), securityCode);
				})
				.isPresent();
	}

	/**
	 * 비밀번호 변경
	 * @param email
	 * @param passwordNew
	 */
	public void updatePassword(String email, String passwordNew) {
		Map<String,Object> paramMap = new HashMap<>();
		paramMap.put("email", email);
		paramMap.put("passwordNew", passwordEncoder.encode(passwordNew));
		paramMap.put("currentDate", new Date());
		accountMapper.updatePassword(paramMap);
	}
	
	public int updateAccountByEmail(AccountDto accountDto) {
		return accountMapper.updateAccountByEmail(accountDto);
	}
	public int updateAccountByAccountId(AccountDto accountDto) {
		return accountMapper.updateAccountByAccountId(accountDto);
	}

	public int getMemberCount(Map<String, Object> searchMap) {
		return accountMapper.getMemberCount(searchMap);
	}

	public List<AccountDto> selectMemberList(Map<String, Object> searchMap) {
		return accountMapper.selectMemberList(searchMap);
	}
}
