package com.curos.service;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor.HSSFColorPredefined;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.curos.dao.ApiMapper;
import com.curos.dto.ApiDto;
import com.curos.exception.BaseException;

@Service
@Transactional
public class ApiService {

	private final Logger logger = LogManager.getLogger();

	@Autowired
	private ApiMapper apiMapper;

	/**
	 * API 리스트 조회
	 * 
	 * @param email
	 * @param clientId
	 * @return
	 * @throws Exception
	 */
	public List<ApiDto> selectApiAllList() {
		return apiMapper.selectApiAllList();
	}

	/**
	 * API 조회
	 * 
	 * @param apiId
	 * @return
	 */
	public ApiDto selectApiByApiId(int apiId) {
		return apiMapper.selectApiByApiId(apiId);
	}

	/**
	 * API 등록
	 * 
	 * @param apiDto
	 * @return
	 * @throws Exception
	 */
	public int insertApi(ApiDto apiDto) throws BaseException {

		logger.info("API 등록 : {}", apiDto.toString());

		int result = apiMapper.insertApi(apiDto);
		apiMapper.insertAccountHasApi(apiDto.getApiId());
		return result;
	}

	/**
	 * API 수정
	 * 
	 * @param apiDto
	 * @return
	 * @throws Exception
	 */
	public int updateApi(ApiDto apiDto) throws BaseException {

		logger.info("API 수정 : {}", apiDto.toString());

		ApiDto org_api = apiMapper.selectApiByApiId(apiDto.getApiId());
		if (org_api.getPrice() != apiDto.getPrice()) {
			apiMapper.insertApiPriceHistory(apiDto);
		}

		return apiMapper.updateApi(apiDto);
	}

	public List<Map<String, Object>> selectAnalysisList(Map<String, Object> paramMap) {
		return apiMapper.selectAnalysisList(paramMap);
	}

	public List<Map<String, Object>> selectAnalysisByAccountId(Map<String, Object> paramMap) {
		return apiMapper.selectAnalysisByAccountId(paramMap);
	}

	public Workbook selectAnalysisDetailByAccountId(Map<String, Object> paramMap) {
		// 워크북 생성
		Workbook wb = new HSSFWorkbook();
		Sheet sheet = wb.createSheet("사용내역");
		Row row = null;
		Cell cell = null;
		int rowNo = 0;
		int cellNo = 0;

		// 테이블 헤더용 스타일
		CellStyle headStyle = wb.createCellStyle();
		// 가는 경계선을 가집니다.
		headStyle.setBorderTop(BorderStyle.THIN);
		headStyle.setBorderBottom(BorderStyle.THIN);
		headStyle.setBorderLeft(BorderStyle.THIN);
		headStyle.setBorderRight(BorderStyle.THIN);
		// 배경색은 노란색입니다.
		headStyle.setFillForegroundColor(HSSFColorPredefined.YELLOW.getIndex());
		headStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		// 데이터는 가운데 정렬합니다.
		headStyle.setAlignment(HorizontalAlignment.CENTER);

		// 헤더 생성
		row = sheet.createRow(rowNo++);
		cell = row.createCell(cellNo++);
		cell.setCellStyle(headStyle);
		cell.setCellValue("날짜");
		cell = row.createCell(cellNo++);
		cell.setCellStyle(headStyle);
		cell.setCellValue("호출 IP");
		cell = row.createCell(cellNo++);
		cell.setCellStyle(headStyle);
		cell.setCellValue("Client Id");
		cell = row.createCell(cellNo++);
		cell.setCellStyle(headStyle);
		cell.setCellValue("API");
		cell = row.createCell(cellNo++);
		cell.setCellStyle(headStyle);
		cell.setCellValue("소스언어");
		cell = row.createCell(cellNo++);
		cell.setCellStyle(headStyle);
		cell.setCellValue("타겟언어");
		cell = row.createCell(cellNo++);
		cell.setCellStyle(headStyle);
		cell.setCellValue("성공여부");
		cell = row.createCell(cellNo++);
		cell.setCellStyle(headStyle);
		cell.setCellValue("가격");
		
		List<Map<String, Object>> detailList = apiMapper.selectAnalysisDetailByAccountId(paramMap);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (Map<String, Object> vo : detailList) {
			row = sheet.createRow(rowNo++);
			cellNo = 0;
			row.createCell(cellNo++).setCellValue((String) sdf.format(vo.get("startDate")).toString());
			row.createCell(cellNo++).setCellValue((String) vo.get("reqIp"));
			row.createCell(cellNo++).setCellValue((String) vo.get("clientId"));
			row.createCell(cellNo++).setCellValue((String) vo.get("apiName"));
			row.createCell(cellNo++).setCellValue((String) vo.get("srcLang"));
			row.createCell(cellNo++).setCellValue((String) vo.get("tgtLang"));
			row.createCell(cellNo++).setCellValue((String) vo.get("successYn"));
			row.createCell(cellNo++).setCellValue((int) vo.get("apiPrice"));
		}

		return wb;
	}
}
