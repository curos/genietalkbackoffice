package com.curos.constant;

/**
 * Role
 */
public enum Role {

	ROLE_ADMIN(1),
	ROLE_DEVELOP(2);

	private int roleId;

	private Role(int roleId) {
		this.roleId = roleId;
	}

	public int getRoleId() {
		return roleId;
	}
}
