package com.curos.constant;

public class UrlConstant {
	public static final String ADD = "Reg";
	public static final String DELETE = "Del";
	public static final String LIST = "List";
	public static final String UPDATE = "Modi";
	public static final String DETAIL = "Detail";
	public static final String ANALY = "Analy";

	// MemberController
	public static final String MAIN = "/main";

	public static final String LOGIN = "/login";
	public static final String LOGOUT = "/logout";
	public static final String CHECK_PASSWORD = "/checkPw";
	public static final String CHNAGE_PASSWORD = "/changePw";
	public static final String CONFIRM_PASSWORD = "/confirmPw";
	public static final String FORGOT_PASSWORD = "/forgotPw";

	public static final String ACCOUNT = "/account";
	public static final String ACCOUNT_DETAIL = ACCOUNT + DETAIL;
	public static final String ACCOUNT_UPDATE = ACCOUNT + UPDATE;

	public static final String MEMBER = "/member";
	public static final String MEMBER_LIST = MEMBER + LIST;
	public static final String MEMBER_ADD = MEMBER + ADD;
	public static final String MEMBER_DETAIL = MEMBER + DETAIL;
	public static final String MEMBER_UPDATE = MEMBER + UPDATE;

	// ApiController
	public static final String API = "/api";
	public static final String API_LIST = API + LIST;
	public static final String API_ADD = API + ADD;
	public static final String API_DETAIL = API + DETAIL;
	public static final String API_UPDATE = API + UPDATE;

	public static final String MEMBER_API = "/memberApi";
	public static final String MEMBER_API_LIST = MEMBER_API + LIST;
	public static final String MEMBER_API_DETAIL = MEMBER_API + DETAIL;
	public static final String MEMBER_API_DETAIL_LIST = MEMBER_API + DETAIL + LIST;

	// ForumController
	public static final String FORUM = "/forum";
	public static final String FORUM_LIST = FORUM + LIST;
	public static final String FORUM_ADD = FORUM + ADD;
	public static final String FORUM_DETAIL = FORUM + DETAIL;
	public static final String FORUM_UPDATE = FORUM + UPDATE;
	public static final String FORUM_DELETE = FORUM + DELETE;
	public static final String FORUM_ANSWER = FORUM + "Answer";
}
