package com.curos.constant;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.ModelAndView;

/**
 * 웹 페이지
 */
public enum WebPage {

	INDEX("/", "Hello World", null, "index")
	,LOGIN(UrlConstant.LOGIN, "로그인", null, "login")
	,MAIN(UrlConstant.MAIN, "Main Page", null, "main/main")
	
	,ACCOUNT_DETAIL(UrlConstant.ACCOUNT_DETAIL, "개인정보", null, "account/detail")
	,ACCOUNT_UPDATE(UrlConstant.ACCOUNT_UPDATE, "개인정보", "개인 정보 수정", "account/update")
	,CHECK_PASSWORD(UrlConstant.CHECK_PASSWORD, "관리자 비밀번호 재확인", null, "main/checkPw")
	
	,MEMBER_LIST(UrlConstant.MEMBER_LIST, "회원 관리", "목록", "member/list")
	,MEMBER_ADD(UrlConstant.MEMBER_ADD, "회원 관리", "추가", "member/add")
	,MEMBER_DETAIL(UrlConstant.MEMBER_DETAIL, "회원 관리", "상세", "member/detail")
	,MEMBER_UPDATE(UrlConstant.MEMBER_UPDATE, "회원 관리", "수정", "member/update")
	
	,API_LIST(UrlConstant.API_LIST, "API 관리", "목록", "api/list")
	,API_ADD(UrlConstant.API_ADD, "API 관리", "추가", "api/add")
	,API_DETAIL(UrlConstant.API_DETAIL, "API 관리", "상세", "api/detail")
	,API_UPDATE(UrlConstant.API_UPDATE, "API 관리", "수정", "api/update")
	
	,MEMBER_API_LIST(UrlConstant.MEMBER_API_LIST, "API 사용량", "목록", "memberApi/list")
	,MEMBER_API_DETAIL(UrlConstant.MEMBER_API_DETAIL, "API 사용량", "상세", "memberApi/detail")
	
	// 개발자 포럼
	,FORUM_LIST(UrlConstant.FORUM_LIST, "개발자포럼", "목록", "forum/list")
	,FORUM_ADD(UrlConstant.FORUM_ADD, "개발자포럼", "등록", "forum/add")
	,FORUM_DETAIL(UrlConstant.FORUM_DETAIL, "개발자포럼", "상세", "forum/detail")
	,FORUM_UPDATE(UrlConstant.FORUM_UPDATE, "개발자포럼", "수정", "forum/update")
	,FORUM_ANSWER(UrlConstant.FORUM_ANSWER, "개발자포럼", "답변", "forum/answer")
	
	// 페이지 찾을 수 없음
	,NOT_FOUND_PAGE(null, null, null, null)
	;
	
	private String identity;
	private String title;
	private String subTitle;
	private String viewName;
	private String menu;
	
	private WebPage(String identity, String title, String subTitle, String viewName) {
		this.identity = identity;
		this.title = title;
		this.subTitle = subTitle;
		this.viewName = viewName;
		this.menu = this.name().toLowerCase();
	}

	public String getIdentity() {
		return identity;
	}

	public String getTitle() {
		return title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public String getViewName() {
		return viewName;
	}
	
	public String getMenu() {
		return menu;
	}

	public boolean isNotFoundPage() {
		return Optional.of(this).filter(wp -> {
			if (StringUtils.isEmpty(wp.getIdentity())
					|| StringUtils.isEmpty(wp.getViewName())) {
				return true;
			}
			return false;
		}).isPresent();
	}
	
	public ModelAndView getModelAndView() {
		ModelAndView mav = new ModelAndView(this.getViewName());
		mav.addObject("lgPanel", this.getTitle());
		mav.addObject("panel", this.getSubTitle());
		mav.addObject("menu", this.getMenu());
		return mav;
	}
	
	public static WebPage getWebPage(String identity) {
		WebPage[] webPages = WebPage.values();
		for (WebPage webPage : webPages) {
			if (StringUtils.equals(webPage.getIdentity(), identity)) {
				return webPage;
			}
			
			// 아래 웹페이지는 Prefix를 검사한다.
			if ( webPage == API_DETAIL || webPage == API_UPDATE ||
					webPage == MEMBER_DETAIL || webPage == MEMBER_UPDATE || webPage == MEMBER_API_DETAIL ||
					webPage == FORUM_DETAIL || webPage == FORUM_UPDATE || webPage == FORUM_ANSWER) {
				if (StringUtils.startsWith(identity, webPage.getIdentity())) {
					return webPage;
				}
			}
		}
		return NOT_FOUND_PAGE;
	}
	
}
