package com.curos.constant;

public class AttributeConstant {
	
	private AttributeConstant() {
		throw new AssertionError("DO NOT CREATE INSTANCE");
	}

	public static final String ACCOUNT_INFO = "accountInfo";
	
	public static final String IS_REDIRECT_CHANGE_PW = "isRedirectChangePw";
	
}
