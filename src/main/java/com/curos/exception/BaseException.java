package com.curos.exception;

@SuppressWarnings("serial")
public class BaseException extends RuntimeException {

	private String errorMsg;
	
	public BaseException(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

}
