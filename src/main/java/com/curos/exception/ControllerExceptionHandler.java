package com.curos.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.curos.dto.CommonResponse;


@RestControllerAdvice
public class ControllerExceptionHandler {

	@ExceptionHandler(BaseException.class)
	public CommonResponse handleException(BaseException exception) {
		CommonResponse response = new CommonResponse();
		response.setError(exception.getErrorMsg());
		return response;
	}
	
}
