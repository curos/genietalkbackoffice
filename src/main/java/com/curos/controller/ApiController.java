package com.curos.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.curos.constant.UrlConstant;
import com.curos.constant.WebPage;
import com.curos.dto.AccountDto;
import com.curos.dto.ApiDto;
import com.curos.dto.CommonResponse;
import com.curos.exception.BaseException;
import com.curos.service.AccountService;
import com.curos.service.ApiService;
import com.curos.util.CustomStringUtils;
import com.curos.util.PagingUtil;

@Controller
public class ApiController {
	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private ApiService apiService;

	@Autowired
	private AccountService accountService;

	/**
	 * API 목록
	 * 
	 * @param webPage
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.API_LIST, method = RequestMethod.GET)
	public ModelAndView apiList(WebPage webPage) {
		List<ApiDto> apiList = apiService.selectApiAllList();
		ModelAndView mav = webPage.getModelAndView();
		mav.addObject("apiList", apiList);
		return mav;
	}

	/**
	 * API 추가 뷰
	 * 
	 * @param webPage
	 * @return
	 */
	@RequestMapping(value = UrlConstant.API_ADD, method = RequestMethod.GET)
	public ModelAndView apiAddView(WebPage webPage) {
		return webPage.getModelAndView();
	}

	/**
	 * API 추가
	 * 
	 * @param model
	 * @param appName
	 * @param appDescription
	 * @param principal
	 * @return
	 * @throws BaseException 
	 */
	@RequestMapping(value = UrlConstant.API_ADD, method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse apiAdd(@RequestParam String apiName, @RequestParam String apiUrl, @RequestParam String price,
			@RequestParam String descript) throws BaseException {

		ApiDto apiDto = new ApiDto();
		apiDto.setApiName(apiName);
		apiDto.setApiUrl(apiUrl);
		apiDto.setDescript(descript);
		
		try {
			int intPrice = Integer.parseInt(price);
			apiDto.setPrice(intPrice);
		} catch (Exception e) {
			throw new BaseException("유효하지 않은 가격 입니다.");
		}
		

		int result = apiService.insertApi(apiDto);

		CommonResponse response = new CommonResponse();
		if (result < 1) {
			response.setError("등록 안됨");
		}

		return response;
	}

	/**
	 * API 상세 뷰
	 * 
	 * @param clientId
	 * @param webPage
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.API_DETAIL + "/{apiId}", method = RequestMethod.GET)
	public ModelAndView apiDetail(@PathVariable int apiId, WebPage webPage) {
		ApiDto apiInfo = apiService.selectApiByApiId(apiId);
		ModelAndView mav = webPage.getModelAndView();

		if (apiInfo != null) {
			String descript = apiInfo.getDescript();
			if (descript != null) {
				apiInfo.setDescript(descript.replaceAll("\r\n", "<br/>"));
			}
		} else {
			mav.addObject("error", "존재하지 않는 데이터입니다.");
			return mav;
		}

		mav.addObject("apiInfo", apiInfo);
		return mav;
	}

	/**
	 * API 수정 뷰
	 * 
	 * @param apiId
	 * @param webPage
	 * @return
	 */
	@RequestMapping(value = UrlConstant.API_UPDATE + "/{apiId}", method = RequestMethod.GET)
	public ModelAndView apiUpdateView(@PathVariable int apiId, WebPage webPage) {
		ApiDto apiInfo = apiService.selectApiByApiId(apiId);
		ModelAndView mav = webPage.getModelAndView();
		if (apiInfo == null) {
			mav.addObject("error", "존재하지 않는 데이터입니다.");
			return mav;
		}
		mav.addObject("apiInfo", apiInfo);
		return mav;
	}

	/**
	 * API 수정
	 * 
	 * @param apiId
	 * @param apiName
	 * @param apiUrl
	 * @param price
	 * @param descript
	 * @param principal
	 * @return
	 * @throws BaseException 
	 */
	@RequestMapping(value = UrlConstant.API_UPDATE + "/{apiId}", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse apiUpdate(@PathVariable int apiId, @RequestParam String apiName, @RequestParam String apiUrl,
			@RequestParam String price, @RequestParam String descript, Principal principal) throws BaseException {
		
		ApiDto apiDto = new ApiDto();
		apiDto.setApiId(apiId);
		apiDto.setApiName(apiName);
		apiDto.setApiUrl(apiUrl);
		apiDto.setDescript(descript);
		
		try {
			int intPrice = Integer.parseInt(price);
			apiDto.setPrice(intPrice);
		} catch (Exception e) {
			throw new BaseException("유효하지 않은 가격 입니다.");
		}
		
		int result = apiService.updateApi(apiDto);
		CommonResponse response = new CommonResponse();
		if (result < 1) {
			response.setError("수정 안됨");
		}

		return response;
	}

	/**
	 * 앱 사용 현황 데이터 조회
	 * 
	 * @param searchDate
	 * @param clientId
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.MEMBER_API_LIST)
	public ModelAndView searchMemberApiList(@RequestParam(required = false) String searchDate,
			@RequestParam(defaultValue = "1") int currPage, WebPage webPage, Principal principal) {
		ModelAndView mav = webPage.getModelAndView();
		int totalCount = accountService.getMemberCount(null);
		PagingUtil page = new PagingUtil(currPage, totalCount);

		Map<String, Object> paramMap = CustomStringUtils.getSearchDate(searchDate);
		paramMap.put("page", page);

		List<Map<String, Object>> memberApiList = apiService.selectAnalysisList(paramMap);

		mav.addObject("memberApiList", memberApiList);
		mav.addObject("start", paramMap.get("startDate"));
		mav.addObject("end", paramMap.get("endDate"));
		mav.addObject("page", page);

		return mav;
	}

	@RequestMapping(value = UrlConstant.MEMBER_API_DETAIL + "/{accountId}")
	public ModelAndView searchMemberApiDetail(@RequestParam(required = false) String searchDate,
			@PathVariable int accountId, WebPage webPage, Principal principal) {
		ModelAndView mav = webPage.getModelAndView();
		AccountDto member = accountService.getAccountByAccountId(accountId);
		if (member == null) {
			mav.addObject("error", "존재하지 않는 데이터입니다.");
			return mav;
		}

		Map<String, Object> paramMap = CustomStringUtils.getSearchDate(searchDate);

		paramMap.put("accountId", accountId);
		List<Map<String, Object>> result = apiService.selectAnalysisByAccountId(paramMap);
		int total = 0;
		int successTotal = 0;
		for (Map<String, Object> map : result) {
			total += ((Long) map.get("cnt")).intValue();
			successTotal += ((BigDecimal) map.get("successCnt")).intValue();
		}

		mav.addObject("result", result);
		mav.addObject("start", paramMap.get("startDate"));
		mav.addObject("end", paramMap.get("endDate"));
		mav.addObject("member", member);
		mav.addObject("total", total);
		mav.addObject("successTotal", successTotal);

		return mav;
	}
	
	@RequestMapping(value = UrlConstant.MEMBER_API_DETAIL + "Download/{accountId}", method = RequestMethod.POST)
	public void searchMemberApiDetailDownload(@RequestParam(required = false) String searchDate,@PathVariable int accountId,
			HttpServletRequest request, HttpServletResponse response) throws IOException {		
		AccountDto member = accountService.getAccountByAccountId(accountId);
		if (member == null) {
			logger.error("사용자를 찾을 수 없습니다. accountId : " + accountId);
			response.setStatus(HttpStatus.NOT_FOUND.value());
			throw new BaseException("사용자를 찾을 수 없습니다.");
		}

		Map<String, Object> paramMap = CustomStringUtils.getSearchDate(searchDate);
		paramMap.put("accountId", accountId);

		Workbook wb = apiService.selectAnalysisDetailByAccountId(paramMap);
		
		// 컨텐츠 타입과 파일명 지정
		String fileName = "API 사용 명세_" + member.getName() + "(" + searchDate + ").xls";
		String header = request.getHeader("User-Agent");

		if (header.contains("MSIE")) {
		    fileName = URLEncoder.encode(fileName,"UTF-8").replaceAll("\\+", "%20");
		    response.setHeader("Content-Disposition", "attachment;filename=" + fileName + ";");
		} else {
		    fileName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
		    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		}
		
		response.setContentType("ms-vnd/excel");
		response.setHeader("Content-Disposition", "attachment;filename="+fileName);

		// 엑셀 출력
		wb.write(response.getOutputStream());
		wb.close();
	}
}
