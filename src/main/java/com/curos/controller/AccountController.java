package com.curos.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.curos.constant.AttributeConstant;
import com.curos.constant.Role;
import com.curos.constant.UrlConstant;
import com.curos.constant.WebPage;
import com.curos.dto.AccountDto;
import com.curos.dto.CommonResponse;
import com.curos.service.AccountService;
import com.curos.service.EmailService;
import com.curos.util.CustomStringUtils;
import com.curos.util.CustomStringUtils.RandomMode;
import com.curos.util.PagingUtil;

import freemarker.template.TemplateException;
import io.reactivex.Observable;

@Controller
public class AccountController {

	private final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private AccountService accountService;

	@Autowired
	private EmailService emailService;

	@RequestMapping("/")
	public ModelAndView index(WebPage webPage) {
		return webPage.getModelAndView();
	}

	/**
	 * 로그인 뷰
	 * 
	 * @param webPage
	 * @return
	 */
	@RequestMapping(value = UrlConstant.LOGIN)
	public ModelAndView login(WebPage webPage) {
		return webPage.getModelAndView();
	}

	/**
	 * 메인 뷰
	 * 
	 * @param model
	 * @param authentication
	 * @param session
	 * @param webPage
	 * @return
	 */
	@RequestMapping(UrlConstant.MAIN)
	public ModelAndView main(Authentication authentication, HttpSession session, WebPage webPage) {

		// TODO : 세션에 사용자 정보 설정 (적당한 위치가 아니다. 변경 필요)
		Object accountInfo = session.getAttribute(AttributeConstant.ACCOUNT_INFO);
		if (Objects.isNull(accountInfo)) {
			AccountDto accountDto = accountService.getAccountByEmail(authentication.getName());
			accountDto.setPassword(null);
			accountDto.setTempPasswd(null);
			accountDto.setTempPasswdDate(null);
			session.setAttribute(AttributeConstant.ACCOUNT_INFO, accountDto);
		}

		/*
		 * 임시 비밀번호 로그인시 비밀번호 변경 화면으로 이동
		 */
		if (Optional.of(authentication)
				.map(Authentication::getDetails)
				.filter(obj -> obj instanceof String)
				.map(obj -> (String)obj)
				.filter(detail -> StringUtils.equals("TEMP_PW_LOGIN", detail))
				.map(detail -> Optional.ofNullable(session.getAttribute(AttributeConstant.IS_REDIRECT_CHANGE_PW)).isPresent())
				.filter(isRedirectChangePw -> isRedirectChangePw == false)
				.isPresent()) {
			session.setAttribute(AttributeConstant.IS_REDIRECT_CHANGE_PW, true);
			return new ModelAndView("redirect:" + UrlConstant.CHECK_PASSWORD);
		}

		return webPage.getModelAndView();
	}

	/**
	 * 비밀번호 분실
	 * 
	 * @param email
	 * @param redirectAttributes
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = UrlConstant.FORGOT_PASSWORD, method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse forgotPw(@RequestParam String email) throws Exception {
		CommonResponse response = new CommonResponse();

		AccountDto account = accountService.getAccountByEmail(email);

		if (account != null && accountService.checkAccountHasRole(account.getAccountId(), Role.ROLE_ADMIN) ) {
			/*
			 * 임시 비밀번호 이메일 발송
			 */
			Observable<String> observable = emailService.sendTempPassword(email);
			observable.blockingSubscribe(
				(tempPassword) -> {
					accountService.updateTempPassword(email, tempPassword);
				}, (throwable) -> {
					throwable.printStackTrace();
					response.setError("임시 비밀번호 발급 실패");
				}, () -> {
					logger.info("임시 비번 발급 프로세스 종료");
					});
		} else {
			response.setError("존재 하지 않는 계정 입니다.");
		}

		return response;
	}

	/**
	 * 비밀번호 확인 뷰
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = UrlConstant.CHECK_PASSWORD, method = RequestMethod.GET)
	public ModelAndView checkPasswordView(HttpSession session, WebPage webPage) {
		ModelAndView mav = webPage.getModelAndView();
		Object isRedirectChangePw = session.getAttribute(AttributeConstant.IS_REDIRECT_CHANGE_PW);
		if(isRedirectChangePw != null && (boolean)isRedirectChangePw == true) {			
			mav.addObject("msg", "발급받은 임시 비밀번호는 변경하셔야 합니다. <br/>");
		}
		return mav;		
	}

	/**
	 * 비밀번호 확인
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = UrlConstant.CHECK_PASSWORD, method = RequestMethod.POST)
	public ModelAndView checkPassword(@RequestParam String password, Principal principal, WebPage webPage, RedirectAttributes attributes) {
		String email = principal.getName();
		if (accountService.equalsPassword(email, password)) {
			attributes.addFlashAttribute("checkPw", "true");
			RedirectView rv = new RedirectView(UrlConstant.ACCOUNT_UPDATE);
			return new ModelAndView(rv);
		} else {
			ModelAndView mav = webPage.getModelAndView();
			mav.addObject("err", 1);
			return mav;
		}
	}

	/**
	 * 비밀번호 변경을 위한 보안코드 발송
	 * 
	 * @param password
	 * @param model
	 * @param principal
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 * @throws TemplateException
	 */

	@RequestMapping(value = UrlConstant.CHNAGE_PASSWORD, method = RequestMethod.GET)
	@ResponseBody
	public CommonResponse changePassword(Principal principal)
			throws MessagingException, IOException, TemplateException {
		CommonResponse response = new CommonResponse();
		String email = principal.getName();
		/*
		 * 보안 코드 발송
		 */
		String securityCode = emailService.sendSecurityCode(email);

		/*
		 * 보안 코드 저장
		 */
		accountService.updateSecurityCode(email, securityCode);
		return response;
	}

	/**
	 * 신규 비밀번호 변경
	 * 
	 * @param passwordOld
	 * @param passwordNew
	 * @param securityCode
	 * @param model
	 * @param principal
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = UrlConstant.CHNAGE_PASSWORD, method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse changePassword(@RequestParam String passwordOld, @RequestParam String passwordNew,
			@RequestParam String securityCode, HttpSession session, Principal principal) {

		CommonResponse response = new CommonResponse();
		String email = principal.getName();
		if (!accountService.checkPassword(email, passwordOld, passwordNew)) {
			response.setError("유효하지 않는 비밀번호 입니다.");
		} else if (!accountService.checkSecurityCode(email, securityCode)) {
			response.setError("유효하지 않는 보안코드 입니다.");
		} else {
			accountService.updatePassword(email, passwordNew);
			session.removeAttribute(AttributeConstant.IS_REDIRECT_CHANGE_PW);
		}

		return response;
	}

	/**
	 * 계인정보 뷰
	 * 
	 * @param model
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.ACCOUNT_DETAIL, method = RequestMethod.GET)
	public ModelAndView accountDetail(Principal principal, WebPage webPage) {
		AccountDto accountDto = accountService.getAccountByEmail(principal.getName());
		ModelAndView mav = webPage.getModelAndView();
		mav.addObject(accountDto);
		return mav;
	}

	/**
	 * 개인정보 수정 뷰
	 * 
	 * @param principal
	 * @param webPage
	 * @return
	 */
	@RequestMapping(value = UrlConstant.ACCOUNT_UPDATE, method = RequestMethod.GET)
	public ModelAndView accountUpdateView(HttpServletRequest req, Principal principal, WebPage webPage) {
		// Password 체크 여부에 따라 안했으면 비밃번호 재확인 페이지로 이동
		Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(req);
		if (flashMap != null && "true".equals((String) flashMap.get("checkPw"))) {
			AccountDto accountDto = accountService.getAccountByEmail(principal.getName());
			ModelAndView mav = webPage.getModelAndView();
			mav.addObject(accountDto);
			return mav;
		} else {
			return new ModelAndView(new RedirectView(UrlConstant.CHECK_PASSWORD));
		}
	}

	/**
	 * 개인정보 수정
	 * 
	 * @param name
	 * @param company
	 * @param mobile
	 * @param telephone
	 * @param postcode
	 * @param address1
	 * @param address2
	 * @param principal
	 * @return
	 */
	@RequestMapping(value = UrlConstant.ACCOUNT_UPDATE, method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse accountUpdate(@RequestParam String name, @RequestParam String company,
			@RequestParam String mobile, @RequestParam String telephone, @RequestParam String postcode,
			@RequestParam String address1, @RequestParam String address2, Principal principal) {

		AccountDto accountDto = new AccountDto();
		accountDto.setEmail(principal.getName());
		accountDto.setName(name);
		accountDto.setCompany(company);
		accountDto.setMobile(mobile);
		accountDto.setTelephone(telephone);
		accountDto.setPostcode(postcode);
		accountDto.setAddress1(address1);
		accountDto.setAddress2(address2);

		int result = accountService.updateAccountByEmail(accountDto);

		CommonResponse response = new CommonResponse();
		if (result < 1) {
			response.setError("수정 안됨");
		}

		return response;
	}

	/**
	 * 회원관리 조회뷰
	 * 
	 * @param currPage
	 * @param webPage
	 * @return
	 */
	@RequestMapping(value = UrlConstant.MEMBER_LIST)
	public ModelAndView memberListView(@RequestParam(defaultValue = "1") int currPage, @RequestParam(required = false) String field,
			@RequestParam(required = false) String keyword, WebPage webPage) {
		ModelAndView mav = webPage.getModelAndView();
		
		Map<String, Object> searchMap = new HashMap<String, Object>();
		searchMap.put("field", field);
		searchMap.put("keyword", keyword);

		int totalCount = accountService.getMemberCount(searchMap);
		PagingUtil page = new PagingUtil(currPage, totalCount);
		searchMap.put("page", page);

		List<AccountDto> memberList = accountService.selectMemberList(searchMap);

		mav.addObject("page", page);
		mav.addObject("field", field);
		mav.addObject("keyword", keyword);
		mav.addObject("memberList", memberList);
		return mav;
	}

	/**
	 * 회원 관리 추가 뷰
	 * 
	 * @param webPage
	 * @return
	 */
	@RequestMapping(value = UrlConstant.MEMBER_ADD, method = RequestMethod.GET)
	public ModelAndView memberAddView(WebPage webPage) {
		return webPage.getModelAndView();
	}

	/**
	 * 회원 관리 추가
	 * 
	 * @param email
	 * @param name
	 * @param company
	 * @param mobile
	 * @param telephone
	 * @param postcode
	 * @param address1
	 * @param address2
	 * @return
	 */
	@RequestMapping(value = UrlConstant.MEMBER_ADD, method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse memberAdd(@RequestParam String email, @RequestParam String name, @RequestParam String company,
			@RequestParam String mobile, @RequestParam String telephone, @RequestParam String postcode,
			@RequestParam String address1, @RequestParam String address2) {
		CommonResponse response = new CommonResponse();

		if (accountService.isExistAccount(email)) {
			response.setError("이미 등록된 Email 주소입니다.");
			return response;
		}

		AccountDto accountDto = new AccountDto();
		accountDto.setEmail(email);
		accountDto.setName(name);
		accountDto.setCompany(company);
		accountDto.setMobile(mobile);
		accountDto.setTelephone(telephone);
		accountDto.setPostcode(postcode);
		accountDto.setAddress1(address1);
		accountDto.setAddress2(address2);
		accountDto.setPassword(CustomStringUtils.random(15, RandomMode.ALL));

		boolean result = accountService.memeberAccountJoin(accountDto);

		if (!result) {
			response.setError("등록 안됨");
		}

		return response;
	}

	/**
	 * 회원 관리 상세 뷰
	 * 
	 * @param accountId
	 * @param webPage
	 * @return
	 */
	@RequestMapping(value = UrlConstant.MEMBER_DETAIL + "/{accountId}", method = RequestMethod.GET)
	public ModelAndView memberDetailView(@PathVariable int accountId, WebPage webPage) {
		ModelAndView mav = webPage.getModelAndView();
		AccountDto member = accountService.getAccountByAccountId(accountId);
		if (member == null) {
			mav.addObject("error", "존재하지 않는 데이터입니다.");
			return mav;
		}
		mav.addObject("member", member);
		return mav;
	}

	/**
	 * 회원 관리 수정 뷰
	 * 
	 * @param accountId
	 * @param webPage
	 * @return
	 */
	@RequestMapping(value = UrlConstant.MEMBER_UPDATE + "/{accountId}", method = RequestMethod.GET)
	public ModelAndView memberUpdateView(@PathVariable int accountId, WebPage webPage) {
		ModelAndView mav = webPage.getModelAndView();
		AccountDto member = accountService.getAccountByAccountId(accountId);
		if (member == null) {
			mav.addObject("error", "존재하지 않는 데이터입니다.");
			return mav;
		}
		mav.addObject("member", member);
		return mav;
	}

	/**
	 * 회원 관리 수정
	 * 
	 * @param accountId
	 * @param email
	 * @param name
	 * @param company
	 * @param mobile
	 * @param telephone
	 * @param postcode
	 * @param address1
	 * @param address2
	 * @return
	 */
	@RequestMapping(value = UrlConstant.MEMBER_UPDATE + "/{accountId}", method = RequestMethod.POST)
	@ResponseBody
	public CommonResponse memberUpdate(@PathVariable int accountId, @RequestParam String email,
			@RequestParam String name, @RequestParam String company, @RequestParam String mobile,
			@RequestParam String telephone, @RequestParam String postcode, @RequestParam String address1,
			@RequestParam String address2) {
		CommonResponse response = new CommonResponse();

		// 만약 email 주소 수정한다면 중복여부 확인해봐야 한다..
		AccountDto member = accountService.getAccountByAccountId(accountId);
		if (member != null && !member.getEmail().equals(email)) {
			if (accountService.isExistAccount(email)) {
				response.setError("이미 가입되어있는 Email 주소입니다.");
				return response;
			}
		}

		AccountDto accountDto = new AccountDto();
		accountDto.setAccountId(accountId);
		accountDto.setEmail(email);
		accountDto.setName(name);
		accountDto.setCompany(company);
		accountDto.setMobile(mobile);
		accountDto.setTelephone(telephone);
		accountDto.setPostcode(postcode);
		accountDto.setAddress1(address1);
		accountDto.setAddress2(address2);

		int result = accountService.updateAccountByAccountId(accountDto);

		if (result < 1) {
			response.setError("수정 안됨");
		}

		return response;
	}
}
